﻿#include "tray.h"

Tray::Tray(QWidget *parent)
    : QWidget(parent)
{
    createTray();
    startServer();
}

Tray::~Tray()
{
    if(QFile::exists(M_CONFIG))
        stopServer();
}

void Tray::createTray()
{
    tray_Menu=new QMenu(this);
    tray_Menu->addAction(QStringLiteral("退出"),QApplication::quit);

    tray=new QSystemTrayIcon(this);
    tray->setIcon(QIcon(":/server.ico"));
    tray->setContextMenu(tray_Menu);
    if(QFile::exists(M_CONFIG))
        tray->setToolTip(QStringLiteral("服务器运行中"));
    else
        tray->setToolTip(QStringLiteral("配置文件缺失，无法启动"));
    tray->show();
}

void Tray::startServer()
{
    Executor *tor=new Executor;
    QThreadPool::globalInstance()->start(tor);
}

void Tray::stopServer()
{
    Terminator *tor=new Terminator;
    QThreadPool::globalInstance()->start(tor);
}
