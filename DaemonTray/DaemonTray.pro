#-------------------------------------------------
#
# Project created by QtCreator 2016-09-17T21:58:38
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = DaemonTray
TEMPLATE = app

include(../qtsingleapplication/src/qtsingleapplication.pri)

INCLUDEPATH += ../../elfproj

SOURCES += main.cpp\
        tray.cpp

HEADERS  += tray.h \
    g_ver.h

RESOURCES += \
    res.qrc

RC_ICONS = server.ico
