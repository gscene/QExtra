﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "DaemonTray"
#define SP_VER 1006
#define SP_UID "{c7d2155f-776a-47cd-8545-67dbdd55e9e0}"
#define SP_TYPE "extra"

#define SP_CFG "DaemonTray.ini"
#define SP_INFO "DaemonTray.json"
#define SP_LINK "DaemonTray.lnk"

/*
 * 1003 使用QDir::setCurrent
 * 1004 使用bat
 * 1005 改为外部调用
*/
#endif // G_VER_H
