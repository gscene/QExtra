﻿#ifndef TRAY_H
#define TRAY_H

#include <QWidget>
#include <QApplication>
#include <QString>
#include <QSettings>
#include <QSystemTrayIcon>
#include <QIcon>
#include <QMenu>
#include <QAction>
#include <QProcess>
#include <QFile>
#include <QRunnable>
#include <QThreadPool>

#define M_CONFIG "../etc/DaemonTray.ini"
#define KEY_START "Daemon/start"
#define KEY_STOP "Daemon/stop"

class Executor : public QRunnable
{
    void run()
    {
        QSettings cfg(M_CONFIG,QSettings::IniFormat);
        QString cmd=cfg.value(KEY_START).toString();
        if(!cmd.isEmpty())
            QProcess::execute(cmd);
    }
};

class Terminator : public QRunnable
{
    void run()
    {
        QSettings cfg(M_CONFIG,QSettings::IniFormat);
        QString cmd=cfg.value(KEY_STOP).toString();
        if(!cmd.isEmpty())
            QProcess::execute(cmd);
    }
};

class Tray : public QWidget
{
    Q_OBJECT

public:
    Tray(QWidget *parent = 0);
    ~Tray();

    void createTray();
    void startServer();
    void stopServer();

private:
    QSystemTrayIcon *tray;
    QMenu *tray_Menu;
    QAction *tray_exitAction;
};

#endif // TRAY_H
