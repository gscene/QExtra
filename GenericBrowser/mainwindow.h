﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "support/sp_env.h"
#include "m_fhs.h"
#include <element/basereceiver.h>
#include <QWebEngineView>

#define PRE_SEARCH "search"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void createMenu();
    void createNavbar();
    void createAppBar();
    void updateFavoriteBar();
    void updateMainBar();
    void updateSearchBar();
    void loadHomePage();
    void startReceiver();
    void stopReceiver();

    bool tickUp(const QString &label,const QString &detail);

    void load(const QString &url);
    void loadUrl(const QString &url);

    void onLoadStart();
    void onLoadFinished(bool flag);
    //   void onUrlChanged(const QUrl &url);
    void onTitleChanged(const QString &title);
    void onReceived(const QByteArray &data);

    void do_openFile();
    void do_openUrl();
    void do_updateNavbar();
    void do_setHomePage();
    void openUrlExternal();
    void parseUrl();

    void processFavoriteAction(QAction *action);
    void processMainAction(QAction *action);
    void processSearchAction(QAction *action);

protected:
    void contextMenuEvent(QContextMenuEvent *)
    {
        menu->exec(QCursor::pos());
    }

private:
    Ui::MainWindow *ui;

    QWebEngineView *view;
    QString selectUrl;
    QMenu *menu;
    QString configFile;
    BaseReceiver *receiver;
    QThread recvThread;
    QMap<QString,QString> items;
    QMap<QString,QString> favItems;
    QMap<QString,QString> searchItems;
};

#endif // MAINWINDOW_H
