﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QString userLand=Location::document + APP_ROOT;
    configFile=userLand + QString("/") + SP_CFG;

    view=new QWebEngineView(this);
    connect(view,&QWebEngineView::loadStarted,this,&MainWindow::onLoadStart);
    connect(view,&QWebEngineView::loadFinished,this,&MainWindow::onLoadFinished);
    connect(view,&QWebEngineView::titleChanged,this,&MainWindow::onTitleChanged);
    //  connect(view,&QWebEngineView::urlChanged,this,&MainWindow::onUrlChanged);
    setCentralWidget(view);

    createMenu();
    createNavbar();
    loadHomePage();
    startReceiver();
}

MainWindow::~MainWindow()
{
    stopReceiver();
    delete ui;
}

void MainWindow::createMenu()
{
    menu=new QMenu(this);

    menu->addAction(QStringLiteral("外部打开"),this,&MainWindow::openUrlExternal);
    menu->addSeparator();
    menu->addAction(QStringLiteral("解析"),this,&MainWindow::parseUrl);
}

void MainWindow::parseUrl()
{
    QString host=view->url().host();
    QSqlQuery query;
    query.exec(QString("select detail from %1 where label like '%%2%'")
               .arg(G_LINK).arg(host));
    if(query.next())
    {
        selectUrl=view->url().toString();
        QString detail=query.value("detail").toString();
        QString py_file=P_FHS_SCRIPT + detail;
        if(QFile::exists(py_file))
        {
            QProcess::startDetached("python",QStringList()<< py_file << selectUrl);
        }
    }
}

void MainWindow::openUrlExternal()
{
    QUrl url=view->url();
    if(url.isValid())
        QDesktopServices::openUrl(url);
}

void MainWindow::startReceiver()
{
    receiver=new BaseReceiver(0,true,P_PORT);
    connect(receiver,&BaseReceiver::received,this,&MainWindow::onReceived);
    connect(&recvThread,&QThread::finished,receiver,&QObject::deleteLater);
    receiver->moveToThread(&recvThread);
    recvThread.start();
}

void MainWindow::stopReceiver()
{
    recvThread.quit();
    recvThread.wait();
}

void MainWindow::onReceived(const QByteArray &data)
{
    QJsonDocument doc=QJsonDocument::fromJson(data);
    if(doc.isObject())
    {
        QJsonObject obj=doc.object();
        QString url=obj.value("url").toString();
        if(url != selectUrl)
            return;

        QString label=obj.value("label").toString();
        QString detail=obj.value("detail").toString();
        if(label.isEmpty() || detail.isEmpty())
            return;

        QSqlQuery query;
        query.exec(QString("select label from %1 where detail = '%2'")
                   .arg(G_FETCH).arg(detail));
        if(query.next())
        {
            QString status=view->url().toString() + " [Finish]";
            ui->statusbar->showMessage(status);
        }
        else
        {
            qApp->clipboard()->setText(detail);

            query.prepare(QString("insert into %1 (label,detail,addition) values (?,?,?)")
                          .arg(G_FETCH));
            query.addBindValue(label);
            query.addBindValue(detail);
            query.addBindValue(url);
            if(!query.exec())
                qDebug()<< query.lastError().text();
        }
    }
}

void MainWindow::loadHomePage()
{
    QSettings cfg(configFile,QSettings::IniFormat);
    cfg.setIniCodec("UTF-8");
    QString home=cfg.value("Main/home").toString();
    if(!home.isEmpty())
        loadUrl(home);
}

void MainWindow::createNavbar()
{
    createAppBar();
    updateFavoriteBar();
    updateMainBar();
    updateSearchBar();
}

void MainWindow::createAppBar()
{
    QMenu *menu=menuBar()->addMenu(QStringLiteral("文件(&F)"));
    menu->addAction(QStringLiteral("打开网址(&W)"),this,&MainWindow::do_openUrl,QKeySequence("Ctrl+W"));
    menu->addAction(QStringLiteral("打开文件(&O)"),this,&MainWindow::do_openFile,QKeySequence("Ctrl+O"));
    menu->addSeparator();
    menu->addAction(QStringLiteral("设为首页"),this,&MainWindow::do_setHomePage);
    menu->addAction(QStringLiteral("更新菜单栏"),this,&MainWindow::do_updateNavbar);
    menu->addSeparator();
    menu->addAction(QStringLiteral("退出(&W)"),this,&MainWindow::close,QKeySequence("Ctrl+Q"));
}

void MainWindow::updateFavoriteBar()
{
    if(!favItems.isEmpty())
        favItems.clear();

    QSqlQuery query;
    query.exec(QString("SELECT COUNT(time_) from %1")
               .arg(P_TRACE));
    if(query.next())
    {
        int count=query.value(0).toInt();
        if(count < 2)
            return;

        QMenu *menu=menuBar()->addMenu(QStringLiteral("收藏夹(&B)"));
        query.exec(QString("SELECT label,COUNT(label) as tick from %1 GROUP BY label ORDER BY tick DESC LIMIT 20")
                   .arg(P_TRACE));
        while (query.next()) {
            QString label=query.value(0).toString();
            if(label.isEmpty())
                continue;
            QString detail=query.value(1).toString();
            menu->addAction(label);
            favItems.insert(label,detail);
        }

        if(!favItems.isEmpty())
            connect(menu,&QMenu::triggered,this,&MainWindow::processFavoriteAction);
    }
}

void MainWindow::updateMainBar()
{
    if(!items.isEmpty())
        items.clear();

    QSqlQuery query;
    query.exec(QString("select label from %1 where category='root'")
               .arg(P_NAVBAR));
    while (query.next()) {
        QString category=query.value(0).toString();
        QMenu *menu=menuBar()->addMenu(category);

        QSqlQuery query;
        query.exec(QString("select label,detail from %1 where category='%2'")
                   .arg(P_SITE)
                   .arg(category));
        while(query.next())
        {
            QString label=query.value(0).toString();
            if(label.isEmpty())
                continue;

            QString detail=query.value(1).toString();
            menu->addAction(label);
            items.insert(label,detail);
        }
        connect(menu,&QMenu::triggered,this,&MainWindow::processMainAction);
    }
}

void MainWindow::updateSearchBar()
{
    if(!searchItems.isEmpty())
        searchItems.clear();

    QMenu *menu=menuBar()->addMenu(QStringLiteral("搜索(&S)"));
    QSqlQuery query;
    query.exec(QString("select label,detail from %1 where category='%2'")
               .arg(TD_PREFIX)
               .arg(PRE_SEARCH));
    while (query.next()) {
        QString label=query.value(0).toString();
        if(label.isEmpty())
            continue;
        QString detail=query.value(1).toString();
        menu->addAction(label);
        searchItems.insert(label,detail);
    }
    if(!searchItems.isEmpty())
        connect(menu,&QMenu::triggered,this,&MainWindow::processSearchAction);
}

void MainWindow::processFavoriteAction(QAction *action)
{
    QString label=action->text();
    QString detail=items.value(label);
    loadUrl(detail);
}

bool MainWindow::tickUp(const QString &label, const QString &detail)
{
    if(label.isEmpty())
        return false;

    QSqlQuery query;
    QString sql=QString("insert into %1 (time_,label,detail) values (?,?,?)").arg(P_TRACE);
    query.prepare(sql);
    query.addBindValue(CurrentDateTime);
    query.addBindValue(label);
    query.addBindValue(detail);
    if(!query.exec())
    {
        qDebug() << query.lastError().text();
        return false;
    }
    else
        return true;
}

void MainWindow::processMainAction(QAction *action)
{
    QString label=action->text();
    QString detail=items.value(label);
    loadUrl(detail);
}

void MainWindow::processSearchAction(QAction *action)
{
    QString input=QInputDialog::getText(this,QStringLiteral("输入关键字"),QStringLiteral("关键字："));
    if(input.isEmpty())
        return;

    QString prefix=searchItems.value(action->text());
    loadUrl(prefix + input);
}

void MainWindow::load(const QString &url)
{
    view->load(QUrl::fromUserInput(url));
}

void MainWindow::loadUrl(const QString &url)
{
    view->load(QUrl(url));
}

void MainWindow::onTitleChanged(const QString &title)
{
    setWindowTitle(title);
}

void MainWindow::onLoadStart()
{
    ui->statusbar->showMessage(QStringLiteral("页面加载中……"));
}

void MainWindow::onLoadFinished(bool flag)
{
    if(flag)
    {
        QString title=view->title();
        QString url=view->url().toString();
        tickUp(title,url);
        ui->statusbar->showMessage(url);
    }
}

void MainWindow::do_setHomePage()
{
    QString home=view->url().toString();
    if(home.isEmpty())
        return;

    QSettings cfg(configFile,QSettings::IniFormat);
    cfg.setIniCodec("UTF-8");
    cfg.setValue("Main/home",home);
}

void MainWindow::do_openUrl()
{
    QString input=QInputDialog::getText(this,QStringLiteral("输入网址"),QStringLiteral("网址："));
    if(input.isEmpty())
        return;

    load(input);
}

void MainWindow::do_openFile()
{
    QUrl url=QFileDialog::getOpenFileUrl(this);
    if(url.isEmpty())
        return;

    view->load(url);
}

void MainWindow::do_updateNavbar()
{
    menuBar()->clear();
    createNavbar();
}
