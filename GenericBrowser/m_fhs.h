﻿#ifndef M_FHS_H
#define M_FHS_H

#include "head/m_head.h"

#define P_NAVBAR "p_navbar"
// 收藏夹
// 书签
// 历史记录
#define P_BOOKMARKS "p_bookmark"
#define P_SITE "p_site"
#define P_TRACE "p_site_trace"

#define P_PORT 23390

#define G_FETCH "generic_fetch"
#define G_LINK "generic_link"

#define P_FHS_SCRIPT "../lib/script/"

#endif // M_FHS_H
