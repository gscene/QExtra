﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "GenericBrowser"
#define SP_VER 1002
#define SP_UID "{835411f0-6b55-4c6c-a4db-771e32613149}"
#define SP_TYPE "shell"

#define SP_CFG "GenericBrowser.ini"
#define SP_INFO "GenericBrowser.json"
#define SP_LINK "GenericBrowser.lnk"

/*
 * 1001
 * 1002 based on QuickBrowser
 * 1003 menu,openUrlExternal,parseUrl
*/

#endif // G_VER_H
