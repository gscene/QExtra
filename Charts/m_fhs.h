﻿#ifndef M_FHS_H
#define M_FHS_H

#include <QString>
#include <QStringList>
#include <QFile>
#include <QTextStream>

#include <QDate>
#include <QDateTime>

#include <QPainter>
#include <QPoint>
#include <QPointF>

#include <QChart>
#include <QChartView>
#include <QLegend>      //图例

#include <QLineSeries>
#include <QBarSet>
#include <QBarSeries>
#include <QBarCategoryAxis>
#include <QAreaSeries>
#include <QLinearGradient>
#include <QBoxPlotSeries>
#include <QPieSeries>
#include <QPieSlice>
#include <QSplineSeries>
#include <QValueAxis>
#include <QPolarChart>
#include <QDateTimeAxis>

using namespace QtCharts;

#endif // M_FHS_H
