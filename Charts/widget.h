﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "areachart.h"

#include <QStringListModel>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

    void invoke(int index);

private slots:
    void on_listView_doubleClicked(const QModelIndex &index);

private:
    Ui::Widget *ui;

    QStringList listName;
    QStringListModel *model;
};

#endif // WIDGET_H
