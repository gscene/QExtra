﻿#ifndef LINECHART_H
#define LINECHART_H

//简易折线图
#include "m_fhs.h"

QChartView *getLineChartView()
{
    QLineSeries *series=new QLineSeries();
    series->append(0,6);
    series->append(2,4);
    series->append(4,8);
    series->append(6,10);
    series->append(8,10);

    QChart *chart=new QChart();
    chart->legend()->hide();
    chart->addSeries(series);
    chart->createDefaultAxes();
    chart->setTitle("Simple Line Chart");

    QChartView *view=new QChartView(chart);
    view->setRenderHint(QPainter::Antialiasing);
    return view;
}

#endif // LINECHART_H
