﻿#ifndef DONUTCHART_H
#define DONUTCHART_H

//圆环图
#include "m_fhs.h"
#include <QFont>

QChartView *getDonutChart()
{

    QPieSeries *series = new QPieSeries();
    series->setHoleSize(0.35);
    series->append("Protein 4.2%", 4.2);
    QPieSlice *slice = series->append("Fat 15.6%", 15.6);
    slice->setExploded();
    slice->setLabelVisible();
    series->append("Other 23.8%", 23.8);
    series->append("Carbs 56.4%", 56.4);

    QChart *chart = new QChart();
    chart->addSeries(series);
    chart->setTitle("Donut with a lemon glaze (100g)");
    chart->legend()->setAlignment(Qt::AlignBottom);
    chart->setTheme(QChart::ChartThemeBlueCerulean);
    chart->setFont(QFont("Arial", 7));

    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    return chartView;
}

#endif // DONUTCHART_H
