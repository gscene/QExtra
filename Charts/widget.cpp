﻿#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    model=new QStringListModel(this);
    ui->listView->setModel(model);

    listName << "Area Chart" << "Bar Chart"
             << "Box Plot Chart" << "Datetime Chart"
             << "Donut Chart" << "Dynamic Spline"
             << "Line Chart" << "Pie Chart"
             << "Polar Chart" << "Spline Chart";

    model->setStringList(listName);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_listView_doubleClicked(const QModelIndex &index)
{
    if(index.isValid())
    {
        QString name=model->data(index).toString();
        int index=listName.indexOf(name);
        invoke(index);
    }
}

void Widget::invoke(int index)
{
    switch (index) {
    case -1:
        break;
    case 0:
        ui->container->addWidget(getAreaChartView());
        break;
    }
}
