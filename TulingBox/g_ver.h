﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "TulingBox"
#define SP_VER 1005
#define SP_UID "{2933f6b2-61b1-45de-8fb2-ee98a8dffd75}"
#define SP_TYPE "shell"

#define SP_CFG "TulingBox.ini"
#define SP_INFO "TulingBox.json"
#define SP_LINK "TulingBox.lnk"

/*
 * 1002 重建结构
 * 1003 优化
 * 1004 单例
*/


#endif // G_VER_H
