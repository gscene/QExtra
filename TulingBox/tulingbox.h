﻿#ifndef TULINGBOX_H
#define TULINGBOX_H

#include "support/sp_env.h"
#include "head/g_functionbase.h"
#include "head/m_function.h"

#include <QTextCursor>
#include <QTextCharFormat>

#include "element/bookmarks.h"
#include "element/commandstore.h"
#include "common/user.h"

#define BASE_URL "http://www.tuling123.com/openapi/api"
#define AI_KEY "cb7463467f4c4f5fbb7abd6464f2e287"

#define T_TEXT 100000
#define T_URL 200000
#define T_NEWS 302000
#define T_COOKBOOK 308000

#define VIEW_NUM 20

#define LINKS QStringLiteral("<a href=\"%1\">%2</a>")

namespace Ui {
class TulingBox;
}

class TulingBox : public QDialog
{
    Q_OBJECT

public:
    explicit TulingBox(QWidget *parent = 0);
    ~TulingBox();

    void replyFinished(QNetworkReply *reply);
    void httpGet(const QString &url);
    void httpPost(const QString &url,const QByteArray &data);
    void ping();
    void say(const QString &word);
    void append(const QString &word);
    void clear();

    void request(const QString &_input);
    void processLink(const QString &link);
    void processNews(const QJsonArray &array);
    void processCookbook(const QJsonArray &array);

    bool db_try();
    void executeCommand(int code);

protected slots:
    void un_reach();

private slots:
    void on_btn_submit_clicked();

private:
    Ui::TulingBox *ui;

    QSqlDatabase db;
    CommandStore cmdStore;
    Bookmarks bookmarks;
    User user;

    QString base_url;
    QString key;
    QNetworkAccessManager *manager;
    QNetworkReply *pingReply;
    QTextCharFormat defaultTextFormat;

    QString _input;
};

#endif // TULINGBOX_H
