#-------------------------------------------------
#
# Project created by QtCreator 2017-03-28T18:33:23
#
#-------------------------------------------------

QT       += core gui sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = TulingBox
TEMPLATE = app

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj
INCLUDEPATH += ../common

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += main.cpp\
    tulingbox.cpp \
    ../../elfproj/support/sp_env.cpp

HEADERS  += tulingbox.h \
    ../../elfproj/support/sp_env.h \
    ../common/m_fhs.h \
    g_ver.h \
    ../../elfproj/element/bookmarks.h \
    ../../elfproj/element/commandstore.h \
    ../../elfproj/common/user.h

FORMS += \
    tulingbox.ui

RESOURCES += \
    res.qrc

RC_ICONS = box.ico
