#-------------------------------------------------
#
# Project created by QtCreator 2018-12-23T09:28:45
#
#-------------------------------------------------

QT       += network sql
QT       -= gui

TARGET = Core
TEMPLATE = lib

DEFINES += CORE_LIBRARY

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        core.cpp

HEADERS += \
        core.h \
        core_global.h 

unix {
    target.path = /usr/lib
    INSTALLS += target
}
