#include "facade.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Facade w;
    w.show();

    return a.exec();
}
