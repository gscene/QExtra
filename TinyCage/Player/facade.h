#ifndef FACADE_H
#define FACADE_H

#include <QMainWindow>

namespace Ui {
class Facade;
}

class Facade : public QMainWindow
{
    Q_OBJECT

public:
    explicit Facade(QWidget *parent = nullptr);
    ~Facade();

private:
    Ui::Facade *ui;
};

#endif // FACADE_H
