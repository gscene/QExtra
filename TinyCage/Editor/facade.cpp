#include "facade.h"
#include "ui_facade.h"

Facade::Facade(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Facade)
{
    ui->setupUi(this);
}

Facade::~Facade()
{
    delete ui;
}
