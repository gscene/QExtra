﻿#ifndef ITEMADD_H
#define ITEMADD_H

#include <head/g_pch.h>

namespace Ui {
class ItemAdd;
}

class ItemAdd : public QDialog
{
    Q_OBJECT

public:
    explicit ItemAdd(QWidget *parent, const QString &table);
    ~ItemAdd();

    bool addItem(const QString &label, const QString &tag,int rate,
                 const QString &detail, const QString &addition);

private slots:
    void on_btn_submit_clicked();

    void on_rate_valueChanged(int value);

private:
    Ui::ItemAdd *ui;
    QString _table;
};

#endif // ITEMADD_H
