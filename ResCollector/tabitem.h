﻿#ifndef TABITEM_H
#define TABITEM_H

#include "restype.h"
#include <common/baseeditor.h>
#include "itemadd.h"

namespace Ui {
class TabItem;
}

class TabItem : public BaseEditor
{
    Q_OBJECT

public:
    explicit TabItem(QWidget *parent,ResType::TYPE type);
    ~TabItem();

    void updateView();
    void newItem();
    void removeItem();
    void edit_on();
    void edit_off();

private slots:
    void on_tableView_doubleClicked(const QModelIndex &index);

private:
    Ui::TabItem *ui;
    ResType::TYPE _type;
};

#endif // TABITEM_H
