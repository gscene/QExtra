﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "ResCollector"
#define SP_VER 1005
#define SP_UID "{fdc6bbd9-9943-42b0-948c-8bf69a5972eb}"
#define SP_TYPE "extra"
#define SP_VERSION "[Version " + QString::number(SP_VER) + QString("]")

#define SP_CFG "ResCollector.ini"
#define SP_INFO "ResCollector.json"
#define SP_LINK "ResCollector.lnk"

/*
 * 1002
 * 1003 restype
 * 1004 tabsClosable
 * 1005 detail,addition
*/


#endif // G_VER_H
