﻿#ifndef M_FHS_H
#define M_FHS_H

#include <head/m_head.h>

//label 标记
//detail 简介
//tag,rate(-1,5)
//addition 下载链接

#define TD_RES_ANIME "res_anime"
#define TD_RES_COMIC "res_comic"
#define TD_RES_GAME "res_game"
#define TD_RES_NOVEL "res_novel"

#define TD_RES_PICTURE "res_picture"
#define TD_RES_MUSIC "res_music"
#define TD_RES_VIDEO "res_video"
#define TD_RES_SOFTWARE "res_software"

//未分类资源
#define TD_RES_POOL "res_pool"

//远程位置
#define TD_RES_URL "res_url"

//本地位置
#define TD_RES_LOCATION "res_location"



#endif // M_FHS_H
