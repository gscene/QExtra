﻿#ifndef WIDGET_H
#define WIDGET_H

#include <head/g_pch.h>
#include "tabitem.h"
#include <string>

namespace Ui {
class Widget;
}

class Widget : public BaseEditor
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

    void updateView();
    void generateMenu();
    void resetTab();
    void addTab(const QString &label);
    QStringList loadOrder();
    void saveOrder();
    void tabSwitch();

private slots:
    void on_tabWidget_tabCloseRequested(int index);

private:
    Ui::Widget *ui;
    QMetaEnum metaEnum;
};

#endif // WIDGET_H
