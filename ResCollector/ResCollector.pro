#-------------------------------------------------
#
# Project created by QtCreator 2017-10-03T23:02:40
#
#-------------------------------------------------

QT       += core gui sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = ResCollector
TEMPLATE = app

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        main.cpp \
        widget.cpp \
    tabitem.cpp \
    ../../elfproj/support/sp_env.cpp \
    itemadd.cpp

HEADERS += \
        widget.h \
    g_ver.h \
    m_fhs.h \
    tabitem.h \
    ../../elfproj/common/baseeditor.h \
    ../../elfproj/support/sp_env.h \
    itemadd.h \
    restype.h

FORMS += \
        widget.ui \
    tabitem.ui \
    itemadd.ui

RESOURCES += \
    res.qrc

RC_ICONS = dock.ico
