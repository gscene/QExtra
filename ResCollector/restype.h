﻿#ifndef RESTYPE_H
#define RESTYPE_H

#include <QObject>
#include <QString>
#include <QStringList>
#include "m_fhs.h"

class ResType : public QObject
{
    Q_OBJECT
public:
    ResType(QObject *parent = 0) : QObject(parent) {}

    enum TYPE
    {
        Software=30,
        URL=91,
        Pool=90,
        Video=21,
        Music=20,
        Anime=10,
        Comic=11,
        Game=12,
        Novel=13,
        Picture=22,
        Location=92
    };
    Q_ENUM(TYPE)

    static QStringList types()
    {
        QStringList order;
        order << "Software" << "URL" << "Pool"
              << "Video" << "Music" << "Picture"
              << "Anime" << "Comic" << "Game"
              << "Novel" << "Location";
        return order;
    }
};

#endif // RESTYPE_H
