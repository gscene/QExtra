﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    int x=event->x();
    int y=event->y();
    ui->statusBar->showMessage(QString::number(x) + "," + QString::number(y));
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_Up:
        QCursor::setPos(QCursor::pos().x(),QCursor::pos().y() - 10);
        break;
    case Qt::Key_Down:
        QCursor::setPos(QCursor::pos().x(),QCursor::pos().y() + 10);
        break;
    case Qt::Key_Left:
        QCursor::setPos(QCursor::pos().x() - 10,QCursor::pos().y());
        break;
    case Qt::Key_Right:
        QCursor::setPos(QCursor::pos().x() + 10,QCursor::pos().y());
        break;
    case Qt::Key_W:
        QCursor::setPos(QCursor::pos().x(),QCursor::pos().y() - 10);
        break;
    case Qt::Key_S:
        QCursor::setPos(QCursor::pos().x(),QCursor::pos().y() + 10);
        break;
    case Qt::Key_A:
        QCursor::setPos(QCursor::pos().x() - 10,QCursor::pos().y());
        break;
    case Qt::Key_D:
        QCursor::setPos(QCursor::pos().x() + 10,QCursor::pos().y());
        break;
    }
}

void MainWindow::on_actionFullScreen_triggered()
{
    showFullScreen();
}

void MainWindow::on_actionNormal_triggered()
{
    showNormal();
}
