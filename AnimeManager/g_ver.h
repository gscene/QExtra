﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "AnimeManager"
#define SP_VER 1003
#define SP_UID "{d5b03967-97ce-4aa4-8f31-f3bc12334dc1}"
#define SP_TYPE "extra"
#define SP_VERSION "[Version " + QString::number(SP_VER) + QString("]")

#define SP_CFG "AnimeManager.ini"
#define SP_INFO "AnimeManager.json"
#define SP_LINK "AnimeManager.lnk"

/*
 * 1002
 * 1003 fetcher
*/

#endif // G_VER_H
