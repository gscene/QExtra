#-------------------------------------------------
#
# Project created by QtCreator 2017-11-28T12:20:27
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AnimeManager
TEMPLATE = app

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        main.cpp \
        widget.cpp \
    ../GenericManager/itemadd.cpp \
    ../GenericManager/fetcher.cpp \
    ../../elfproj/support/sp_env.cpp

HEADERS += \
        widget.h \
    g_ver.h \
    m_fhs.h \
    itemeditor.h \
    ../GenericManager/itemadd.h \
    ../GenericManager/fetcher.h \
    ../../elfproj/support/sp_env.h

FORMS += \
        widget.ui \
    ../GenericManager/itemadd.ui \
    ../GenericManager/fetcher.ui
