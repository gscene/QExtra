﻿#ifndef WIDGET_H
#define WIDGET_H

#include "itemeditor.h"
#include "element/locationsynchronizer.h"
#include "../GenericManager/fetcher.h"

namespace Ui {
class Widget;
}

class Widget : public ItemEditor
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

    void updateView();
    void generateMenu();
    void collapseAll();
    void showSearchBox();
    void updateTopicLocation();
    void openTopicLocation();
    void startSynchronizer();
    void stopSynchronizer();

private slots:
    void on_entryView_clicked(const QModelIndex &index);
    void on_entryView_doubleClicked(const QModelIndex &index);

private:
    Ui::Widget *ui;

    QString linkTable;
    QMap<QString,QString> topicLocations;
    QMap<QString,QString> itemDetails;

    Fetcher *fetcher;
    LocationSynchronizer *synchronizer;
    QThread syncThread;
};

#endif // WIDGET_H
