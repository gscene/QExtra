﻿#ifndef M_FHS_H
#define M_FHS_H

#include "head/m_head.h"

#define T_RES_LINK "res_anime_link"
/* Category:
 * Entry 普通条目
 * Location 目录，
 * Site 网站条目，
 * Prefix 搜索条目
*/

#define T_RES_LOCATION "res_anime_location"
#define T_RES_FETCH "res_anime_fetch"

#endif // M_FHS_H
