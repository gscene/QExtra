﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "MUMPicker"
#define SP_VER 1002
#define SP_UID "{UUID}"
#define SP_TYPE "prop"

#define SP_CFG "MUMPicker.ini"
#define SP_INFO "MUMPicker.json"
#define SP_LINK "MUMPicker.lnk"

#endif // G_VER_H
