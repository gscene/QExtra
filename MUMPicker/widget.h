﻿#ifndef WIDGET_H
#define WIDGET_H

#include "common/baseeditor.h"

namespace Ui {
class Widget;
}

class Widget : public BaseEditor
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent,
                    const QString &location);
    ~Widget();

    void updateView();
    void generateMenu();
    void updateTopic();
    void updateTopicByList();
    void openTopicLocation();

private:
    Ui::Widget *ui;

    QString locationTable;
    QString tagTable;

    QString baseDir;
    QStringList imageType;
    QStringList videoType;
};

#endif // WIDGET_H
