﻿#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent, const QString &location) :
    BaseEditor(parent),
    baseDir(location),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    table=T_RES_LOCATION;
    tagTable=T_RES_TAG;
    imageType << "jpg" << "jpeg" << "png" << "bmp";
    videoType << "mp4" << "mkv" << "avi" << "wmv";
}

Widget::~Widget()
{
    delete ui;
}

void Widget::generateMenu()
{
    menu->addAction(QStringLiteral("更新主题"),
                    this,&Widget::updateTopic);
    menu->addAction(QStringLiteral("更新列表..."),
                    this,&Widget::updateTopicByList);
        menu->addSeparator();
    menu->addAction(QStringLiteral("打开目录"),
                    this,&Widget::openTopicLocation);
}

void Widget::updateTopic()
{
    QDir dir(baseDir);
    if(!dir.exists())
        return;

    truncateTable();
    QDirIterator itor(dir,QDirIterator::Subdirectories);
    while (itor.hasNext()) {
        QFileInfo fi=itor.fileInfo();
        QString baseName=fi.baseName();
        QString filePath=fi.filePath();
        QString suffix=fi.suffix();
        if(imageType.contains(suffix))
            addItem("Cover",baseName,filePath);
        else if(videoType.contains(suffix))
            addItem("Body",baseName,filePath);
    }
}

void Widget::updateTopicByList()
{
    QDir dir(baseDir);
    if(!dir.exists())
        return;


}
