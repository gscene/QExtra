#-------------------------------------------------
#
# Project created by QtCreator 2017-12-28T15:20:10
#
#-------------------------------------------------

QT       += core gui sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MUMPicker
TEMPLATE = app

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        main.cpp \
        widget.cpp

HEADERS += \
        widget.h \
    ../../elfproj/common/baseeditor.h \
    m_fhs.h \
    g_ver.h

FORMS += \
        widget.ui
