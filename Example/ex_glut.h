﻿#ifndef EX_GLUT_H
#define EX_GLUT_H

// LIBS += -lGL -lGLU -lglut

#include <QString>
#include <QDebug>
#include <GL/glut.h>

void ex_glut(int argc, char *argv[])
{
    glutInit(&argc,argv);
    int winId=glutCreateWindow("OPenGL Version");
    const GLubyte *name=glGetString(GL_VENDOR);
    const GLubyte *render=glGetString(GL_RENDERER);
    const GLubyte *version=glGetString(GL_VERSION);
    const GLubyte *gluVersion=gluGetString(GLU_VERSION);

    qDebug() << "显卡厂家：" << QString((char*)name);
    qDebug() << "渲染器：" << QString((char*)render);
    qDebug() << "OpenGL版本" << QString((char*)version);
    qDebug() << "GLU版本：" << QString((char*)gluVersion);
    glutDestroyWindow(winId);
    return 0;
}

#endif // EX_GLUT_H
