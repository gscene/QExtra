#ifndef EX_MONGO_H
#define EX_MONGO_H

#define MONGO_SHORT_TAG
#include "element/mongohelper.h"

#include <QString>
#include <QFileInfo>
#include <QDir>
#include <QDirIterator>

void ex_scanToMongo(const QString &location)
{
    MongoHelper mongo;
    mongo.connect();
    mongo.useCollection("generic","resource");
    QDirIterator dirIter(location,QDir::Files,
                         QDirIterator::Subdirectories);
    while (dirIter.hasNext()) {
        dirIter.next();

        std::string dirName=dirIter.fileInfo().dir().dirName().toStdString();
        auto item=document{};
        item << "category" << dirName
             << "label" << dirIter.fileName().toStdString()
             << "detail" << dirIter.filePath().toStdString()
             << "addition" << QString::number(dirIter.fileInfo().size()).toStdString();
        mongo.insertOne(item.view());
    }
}

#endif // EX_MONGO_H
