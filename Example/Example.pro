QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

INCLUDEPATH += ../../elfproj

LIBS += -lyaml-cpp
include(../../elfproj/include/mongo_prefix.pri)


DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        main.cpp

HEADERS += \
    ex_yaml.h \
    ex_glut.h \
    ex_mongo.h

DISTFILES += \
    player.yml
