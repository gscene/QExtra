﻿#ifndef EX_YAML_H
#define EX_YAML_H

#include <QString>
#include <QDebug>
#include "yaml-cpp/yaml.h"

void ex_yaml()
{
    YAML::Node config = YAML::LoadFile("player.yml");
    YAML::Node user=config["User"];
    qDebug() << "Id:" << user["id"].as<int>();

    const string name=user["name"].as<string>();
    qDebug() << "Name:" <<  QString::fromStdString(name);

    const string pos=user["position"].as<string>();
    qDebug() << "Position: " << QString::fromStdString(pos);
    qDebug() << "Level: " << user["properties"]["level"].as<int>();
    qDebug() << "HP: " << user["properties"]["HP"].as<int>();

    YAML::Node props=user["props"];
    qDebug() << "Props:";
    for(size_t i=0;i<props.size();i++)
        qDebug() << " " << QString::fromStdString(props[i]["name"].as<string>())
                << "Num: " << props[i]["num"].as<int>();

    // std::ofstream fout("config.yaml");
    // fout << config;
}

#endif // EX_YAML_H
