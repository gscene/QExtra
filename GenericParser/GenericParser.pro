#-------------------------------------------------
#
# Project created by QtCreator 2018-02-01T19:03:42
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GenericParser
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj

SOURCES += \
        main.cpp \
        widget.cpp

HEADERS += \
        widget.h \
    g_ver.h \
    m_fhs.h \
    ../../elfproj/element/basereceiver.h

FORMS += \
        widget.ui
