﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "GenericParser"
#define SP_VER 1003
#define SP_UID "{6183257a-3123-4513-80d4-d7aac6d0da2c}"
#define SP_TYPE "shell"

#define SP_CFG "GenericParser.ini"
#define SP_INFO "GenericParser.json"
#define SP_LINK "GenericParser.lnk"

/*
 * 1002 parseUrl
*/

#endif // G_VER_H
