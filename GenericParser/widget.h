﻿#ifndef WIDGET_H
#define WIDGET_H

#include <head/g_pch.h>
#include "m_fhs.h"
#include <element/basereceiver.h>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

    void startReceiver();
    void stopReceiver();
    void onReceived(const QByteArray &data);

private slots:
    void on_btn_paste_clicked();
    void on_btn_parser_clicked();

private:
    Ui::Widget *ui;

    QString selectUrl;
    BaseReceiver *receiver;
    QThread recvThread;
};

#endif // WIDGET_H
