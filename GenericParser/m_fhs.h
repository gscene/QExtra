﻿#ifndef M_FHS_H
#define M_FHS_H

#include "head/m_head.h"

#define P_PORT 23390

#define G_FETCH "generic_fetch"
#define G_LINK "generic_link"

#define P_FHS_SCRIPT "../lib/script/"

#endif // M_FHS_H
