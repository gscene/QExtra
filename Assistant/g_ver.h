﻿#ifndef G_VER
#define G_VER

#define SP_NAME "Assistant"
#define SP_VER 1014
#define SP_UID "{d6faaab6-c5a9-46f1-896a-c0b2e062b4c1}"
#define SP_TYPE "shell"

#define SP_CFG "Assistant.ini"
#define SP_INFO "Assistant.json"
#define SP_LINK "Assistant.lnk"

/*
 * 1002
 * 1003 note note2 todo
 * 1004 autosave
 * 1005 todo palette
 * 1006 优化note laod/save
 * 1007 命令识别修正
 * 1008 分离TulingBox
 * 1009 菜单重写
 * 1010 receiver
 * 1011 note save的bug
 * 1012 auto sync
 * 1014 增加了MQTT Subscriber支持
*/

#endif // G_VER
