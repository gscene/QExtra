﻿#ifndef FACADE_H
#define FACADE_H

#include "basefacade.h"

#include "head/g_bootfunction.h"
#include "head/g_function.h"
#include "support/ref_commands.h"
#include "support/basereceiver.h"
#include "support/bookmarks.h"
#include "support/simplesubscriber.h"
#include "support/pie.h"
#include "head/m_function.h"

#include "support/basereceiver.h"
#include "support/baseconfigurator.h"
#include "support/bookmarks.h"

#include "common/user.h"

#ifdef Q_OS_WIN
#include "support/reg_winver.h"
#define TulingBox "TulingBox.exe"
#else
#define TulingBox "TulingBox"
#endif

#include "support/ref_commands.h"
#include "support/bookmarks.h"

#include "module/note.h"
#include "module/note2.h"
#include "module/todo.h"
#include "module/popup.h"

#define skin_def ui->body->setStyleSheet("image: url(:/res/assistant.png)")

#define Calc "calc"

namespace Ui {
class Facade;
}

class Facade : public BaseFacade
{
    Q_OBJECT

public:
    explicit Facade(QWidget *parent = 0);
    ~Facade();

    void keyPressEvent(QKeyEvent *event);
    void timerEvent(QTimerEvent *event);
    void mouseDoubleClickEvent(QMouseEvent *);
    void pop(const QString &label, const QString &detail);

    void startUp();
    void fillUserInfo();
    void startReceiver();
    void startSubscriber();
    void received(const QByteArray &data);
    void onMessageReceived(const QMQTT::Message& message);

    void login();
    void createMenu();
    void generateMenu();
    void reloadMenu();
    void createUserMenu();
    void createOptionMenu();
    void createPropsMenu();

    void createFavoriteMenu();
    void createFavoriteMenuItem();
    void reloadFavoriteMenu();

    void setStartup();
    void unsetStartup();
    void reloadMenuAction();

    bool connectStorage();
    bool db_try();
    void aboutVersion();
    void showHardwareInfo();

    void showNote();
    void showNote2();
    void showTodo();
    void detectTodo();
    void invokeCalc();
    void invokeTulingBox();

    void setUerName();
    void setUserGroup();

    void onTrayStateChanged();
    void processFavoriteAction(QAction *action);
    void processPropsAction(QAction *action);
    void processMenuAction(QAction *action);

private:
    Ui::Facade *ui;

    BaseConfigurator config;
    QSqlDatabase db;
    int autoHide_timer;
    int autoSync_timer;
    int dbTry_failure;

    User user;
    Note *note;
    Note2 *note2;
    Todo *todo;
    Popup *popup;

    QMenu *m_favorite;
    Bookmarks bookmarks;
    SimpleSubscriber *subscriber;

    BaseReceiver *receiver;
    QThread recvThread;

    QMap<QString,QString> menuItems;
    QList<QMenu *> menuList;
};

#endif // FACADE_H
