﻿#ifndef NOTE2_H
#define NOTE2_H

#include "module/autosavedialog.h"
#define e_note21 "/note21.txt"
#define e_note22 "/note22.txt"

namespace Ui {
class Note2;
}

class Note2 : public AutoSaveDialog
{
    Q_OBJECT

public:
    explicit Note2(QWidget *parent, const QString &location);
    ~Note2();

    void load();
    void save();

private:
    Ui::Note2 *ui;

    QString userLand;
    QString fileName_L;
    QString fileName_R;
};

#endif // NOTE2_H
