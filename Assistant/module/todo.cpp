﻿#include "todo.h"
#include "ui_todo.h"

Todo::Todo(QWidget *parent, const QString &location) :
    AutoSaveDialog(parent),
    userLand(location),
    ui(new Ui::Todo)
{
    ui->setupUi(this);

    fileName_=userLand + e_todo;
}

Todo::~Todo()
{
    delete ui;
}

void Todo::load()
{
    // ui->textEdit->setFontPointSize(24);
    // ui->textEdit->setTextColor(QColor("red"));

    if(!QFile::exists(fileName_))
        return;

    QFile file(fileName_);
    file.open(QFile::ReadWrite | QFile::Text);
    QTextStream text(&file);
    QString content=text.readAll();
    file.close();
    if(!content.isEmpty())
        ui->textEdit->setPlainText(content);
}

void Todo::detect()
{   
    if(!QFile::exists(fileName_))
        return;

    QFile file(fileName_);
    file.open(QFile::ReadWrite | QFile::Text);
    QTextStream text(&file);
    QString content=text.readAll();
    file.close();
    if(!content.isEmpty())
    {
        ui->textEdit->setPlainText(content);
        this->show();
    }
}

void Todo::save()
{
    QString content=ui->textEdit->toPlainText();
    QFile file(fileName_);
    file.open(QFile::WriteOnly | QFile::Text);
    QTextStream text(&file);
    text << content;
    file.close();
}
