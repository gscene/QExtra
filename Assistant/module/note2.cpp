﻿#include "note2.h"
#include "ui_note2.h"

Note2::Note2(QWidget *parent, const QString &location) :
    AutoSaveDialog(parent),
    userLand(location),
    ui(new Ui::Note2)
{
    ui->setupUi(this);

    fileName_L=userLand + e_note21;
    fileName_R=userLand + e_note22;
}

Note2::~Note2()
{
    delete ui;
}

void Note2::load()
{
    if(QFile::exists(fileName_L))
    {
        QFile file(fileName_L);
        file.open(QFile::ReadWrite | QFile::Text);
        QTextStream text(&file);
        QString content=text.readAll();
        file.close();

        if(!content.isEmpty())
            ui->textEdit_L->setPlainText(content);
    }

    if(QFile::exists(fileName_R))
    {
        QFile file(fileName_R);
        file.open(QFile::ReadWrite | QFile::Text);
        QTextStream text(&file);
        QString content=text.readAll();
        file.close();

        if(!content.isEmpty())
            ui->textEdit_R->setPlainText(content);
    }
}

void Note2::save()
{
    QString content_L=ui->textEdit_L->toPlainText();
    QFile file_L(fileName_L);
    file_L.open(QFile::WriteOnly | QFile::Text);
    QTextStream text_L(&file_L);
    text_L << content_L;
    file_L.close();

    QString content_R=ui->textEdit_R->toPlainText();
    QFile file_R(fileName_R);
    file_R.open(QFile::WriteOnly | QFile::Text);
    QTextStream text_R(&file_R);
    text_R << content_R;
    file_R.close();
}
