﻿#include "note.h"
#include "ui_note.h"
#include <QDebug>

Note::Note(QWidget *parent, const QString &location) :
    AutoSaveDialog(parent),
    userLand(location),
    ui(new Ui::Note)
{
    ui->setupUi(this);

    fileName_=userLand + e_note;
}

Note::~Note()
{
    delete ui;
}

void Note::load()
{
    if(!QFile::exists(fileName_))
        return;

    QFile file(fileName_);
    file.open(QFile::ReadWrite | QFile::Text);
    QTextStream text(&file);
    QString content=text.readAll();
    file.close();

    if(!content.isEmpty())
        ui->textEdit->setPlainText(content);
}

void Note::save()
{
    QString content=ui->textEdit->toPlainText();
    QFile file(fileName_);
    file.open(QFile::WriteOnly | QFile::Text);
    QTextStream text(&file);
    text << content;
    file.close();
}

void Note::on_in_date_clicked()
{
    ui->textEdit->appendPlainText(QDate::currentDate().toString("yyyy-M-d"));
}

void Note::on_in_time_clicked()
{
    ui->textEdit->appendPlainText(QTime::currentTime().toString());
}

