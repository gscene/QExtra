﻿#ifndef TODO_H
#define TODO_H

#include "autosavedialog.h"
#define e_todo "/todo.txt"

namespace Ui {
class Todo;
}

class Todo : public AutoSaveDialog
{
    Q_OBJECT

public:
    explicit Todo(QWidget *parent, const QString &location);
    ~Todo();

    void load();
    void save();
    void detect();

private:
    Ui::Todo *ui;

    QString userLand;
    QString fileName_;
};

#endif // TODO_H
