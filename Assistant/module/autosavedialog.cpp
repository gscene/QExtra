﻿#include "autosavedialog.h"

AutoSaveDialog::AutoSaveDialog(QWidget *parent):
    QDialog(parent){}

void AutoSaveDialog::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Escape)
        close();
}

void AutoSaveDialog::closeEvent(QCloseEvent *)
{
    save();
}
