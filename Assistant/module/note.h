﻿#ifndef NOTE_H
#define NOTE_H

#include "autosavedialog.h"
#define e_note "/note.txt"

namespace Ui {
class Note;
}

class Note : public AutoSaveDialog
{
    Q_OBJECT

public:
    explicit Note(QWidget *parent, const QString &location);
    ~Note();

    void load();
    void save();

private slots:
    void on_in_date_clicked();
    void on_in_time_clicked();

private:
    Ui::Note *ui;

    QString userLand;
    QString fileName_;
};

#endif // NOTE_H
