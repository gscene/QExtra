﻿#ifndef AUTOSAVEDIALOG_H
#define AUTOSAVEDIALOG_H

#include <QDialog>
#include <QPlainTextEdit>
#include <QTextEdit>

#include "support/sp_env.h"

class AutoSaveDialog : public QDialog
{
public:
    explicit AutoSaveDialog(QWidget *parent = 0);

    virtual void load() = 0;
    virtual void save() = 0;

protected:
    void keyPressEvent(QKeyEvent *event);
    void closeEvent(QCloseEvent *);
};

#endif // AUTOSAVEDIALOG_H
