﻿#include "facade.h"
#include <QtSingleApplication>
#include "head/e_boot.h"

int main(int argc, char *argv[])
{
    SP_Single_Boot

            a.setQuitOnLastWindowClosed(false);

    Facade w;
    a.setActivationWindow(&w);
    w.show();

    return a.exec();
}
