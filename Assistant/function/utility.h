﻿#ifndef UTILITY_H
#define UTILITY_H

#include "support/sp_env.h"
//opt/<>/props.json

class Utility
{
public:
    void insert(const QString &label, const QString &detail);
    bool isEmpty();
    void open(const QString &key);
    void clear();
    bool contains(const QString &key);
    QMap<QString,QString> getItems() const;

private:
    QMap<QString,QString> _items;
};

#endif // UTILITY_H
