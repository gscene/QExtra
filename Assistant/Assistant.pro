#-------------------------------------------------
#
# Project created by QtCreator 2017-03-10T11:27:04
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = Assistant
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH +=D:/usr/include/qmqtt
win32:CONFIG(release, debug|release): LIBS += -LD:/usr/lib/release/ -lqmqtt
else:win32:CONFIG(debug, debug|release): LIBS += -LD:/usr/lib/debug/ -lqmqtt

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj
INCLUDEPATH += ../common

SOURCES += main.cpp\
        facade.cpp \
    module/note.cpp \
    module/note2.cpp \
    module/todo.cpp \
    module/autosavedialog.cpp \
    ../../elfproj/support/sp_env.cpp \
    module/popup.cpp

HEADERS  += facade.h \
    basefacade.h \
    g_ver.h \
    ../../elfproj/common/t_facade.h \
    ../../elfproj/support/sp_env.h \
    module/note.h \
    module/note2.h \
    module/todo.h \
    module/autosavedialog.h \
    ../common/m_fhs.h \
    ../../elfproj/support/basereceiver.h \
    ../../elfproj/common/user.h \
    ../../elfproj/support/baseconfigurator.h \
    ../../elfproj/support/bookmarks.h \
    ../../elfproj/support/commandstore.h \
    ../../elfproj/support/simplesubscriber.h \
    module/popup.h

FORMS    += facade.ui \
    module/note.ui \
    module/note2.ui \
    module/todo.ui \
    module/task.ui \
    module/memo.ui \
    module/popup.ui

RESOURCES += \
    res.qrc

RC_ICONS = app.ico
