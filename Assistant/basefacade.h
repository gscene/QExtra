﻿#ifndef BASEFACADE_H
#define BASEFACADE_H

#include "common/t_facade.h"
#include "m_fhs.h"

class BaseFacade : public TransFacade
{
    Q_OBJECT

public:
    explicit BaseFacade(QWidget *parent = 0):
        TransFacade(parent)
    {
        userLand = Location::document + APP_ROOT;
        configLocation=QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation);
        localLocation=QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
        dataLocation=QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
        cacheLocation=QStandardPaths::writableLocation(QStandardPaths::CacheLocation);

        QDir dir(userLand);
        if(!dir.exists())
            dir.mkpath(userLand);

        if(getUid().isEmpty())
            putUid(QUuid::createUuid().toString());
    }

    ~BaseFacade(){}

    virtual void startUp() = 0;
    virtual void createMenu() = 0;
    virtual void createOptionMenu() = 0;

    QString getUid() const
    {
        QSettings reg(APP_REG,QSettings::NativeFormat);
        return reg.value("uid").toString();
    }
    void putUid(const QString &uid)
    {
        QSettings reg(APP_REG,QSettings::NativeFormat);
        reg.setValue("uid",uid);
    }

protected:
    QString userLand;

    QString configLocation;
    QString localLocation;      //同config
    QString dataLocation;
    QString cacheLocation;
};

#endif // BASEFACADE_H
