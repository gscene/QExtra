﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "GenericManager"
#define SP_VER 1014
#define SP_UID "{918f4baa-0811-4c99-b531-8a85b9732e0d}"
#define SP_TYPE "shell"

#define SP_CFG "GenericManager.ini"
#define SP_INFO "GenericManager.json"
#define SP_LINK "GenericManager.lnk"

/*
 * 1001 项目开始
 * 1002 增加查找功能
 * 1003 产生BaseEditor基类
 * 1005 使用右键播放功能
 * 1007 FHS修改
 * 1008 fetcher分离
 * 1009 #13
 * 1010 #18
 * 1011 统一widget
 * 1012 搜索框
 * 1013 快键键，updateResource
 * 1014 tagViewer
*/

#endif // G_VER_H
