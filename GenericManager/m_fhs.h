﻿#ifndef M_FHS_H
#define M_FHS_H

#include "head/m_head.h"

#define G_LOCATION "generic_location"
#define G_TAG "generic_tag"
#define G_LINK "generic_link"
#define G_FETCH "generic_fetch"

#define ID_COL 0
#define CAT_COL 1
#define LAB_COL 2
#define DET_COL 3
#define ADD_COL 4

#define LAB_WID 480

#endif // M_FHS_H
