﻿#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    BaseEditor(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    viewer=new Viewer(this);
    ui->tabWidget->addTab(viewer,QStringLiteral("查看"));

    tagViewer=new TagViewer(this);
    ui->tabWidget->addTab(tagViewer,QStringLiteral("标签"));

    fetcher=new Fetcher(this);
    ui->tabWidget->addTab(fetcher,QStringLiteral("下载"));
}

Widget::~Widget()
{
    delete ui;
}

