#-------------------------------------------------
#
# Project created by QtCreator 2016-09-21T13:59:50
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = GenericManager
TEMPLATE = app

include(../qtsingleapplication/src/qtsingleapplication.pri)

INCLUDEPATH += ../../elfproj
INCLUDEPATH += module

SOURCES += main.cpp\
    module/fetcher.cpp \
    module/itemadd.cpp \
    ../../elfproj/support/sp_env.cpp \
    widget.cpp \
    module/viewer.cpp \
    module/tagviewer.cpp

HEADERS  += \
    g_ver.h \
    ../../elfproj/common/baseeditor.h \
    m_fhs.h \
    module/fetcher.h \
    module/itemadd.h \
    ../../elfproj/support/sp_env.h \
    widget.h \
    module/viewer.h \
    module/tagviewer.h

FORMS    += \
    module/fetcher.ui \
    module/itemadd.ui \
    widget.ui \
    module/viewer.ui \
    module/tagviewer.ui

RESOURCES += \
    res.qrc

RC_ICONS = manager.ico
