﻿#ifndef MANAGER_H
#define MANAGER_H

#include "viewer.h"
#include "fetcher.h"
#include "tagviewer.h"

namespace Ui {
class Widget;
}

class Widget : public BaseEditor
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private:
    Ui::Widget *ui;

    Viewer *viewer;
    Fetcher *fetcher;
    TagViewer *tagViewer;
};

#endif // MANAGER_H
