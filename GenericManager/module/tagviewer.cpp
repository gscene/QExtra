﻿#include "tagviewer.h"
#include "ui_tagviewer.h"

TagViewer::TagViewer(QWidget *parent) :
    BaseEditor(parent),
    ui(new Ui::TagViewer)
{
    ui->setupUi(this);

    table=G_TAG;
    model=new QSqlTableModel(this);
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    ui->tableView->setModel(model);
    ui->tableView->verticalHeader()->hide();

    updateView();
    createMenu();
}

TagViewer::~TagViewer()
{
    delete ui;
}

void TagViewer::updateView()
{
    model->setTable(table);
    model->select();

    ui->tableView->hideColumn(ID_COL);
    model->setHeaderData(1,Qt::Horizontal,QStringLiteral("名称"));    //label
    ui->tableView->setColumnWidth(1,LAB_WID);
    model->setHeaderData(2,Qt::Horizontal,QStringLiteral("标签"));    //detail
    ui->tableView->hideColumn(3);   //addition
}

void TagViewer::generateMenu()
{
    menu->addAction(QStringLiteral("更新视图 (F5)"),this,&TagViewer::updateView);
    menu->addAction(QStringLiteral("更新标签... (F2)"),this,&TagViewer::updateTag);
    menu->addSeparator();
    QMenu *optMenu=menu->addMenu(QStringLiteral("选项"));
    optMenu->addAction(QStringLiteral("撤销 (Ctrl+Z)"),this,&TagViewer::revert);
    optMenu->addAction(QStringLiteral("保存 (Ctrl+S)"),this,&TagViewer::save);
    optMenu->addSeparator();
    optMenu->addAction(QStringLiteral("删除 (Delete)"),this,&TagViewer::removeItem);
}

void TagViewer::updateTag()
{
    QModelIndex index=ui->tableView->currentIndex();
    if(index.isValid())
    {
        QString tag=sp_fetchString(index,2);
        QString newTag=QInputDialog::getText(this,QStringLiteral("输入新标签"),
                                             QStringLiteral("标签："),
                                             QLineEdit::Normal,tag);
        if(newTag == tag || newTag.isEmpty())
            return;

        int id=sp_fetchId(index);
        if(sp_updateColumnById(table,"detail",newTag,id))
            updateView();
    }
}

void TagViewer::removeItem()
{
    QModelIndex index=ui->tableView->currentIndex();
    model->removeRow(index.row());
}

void TagViewer::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_F5)
        updateView();
    else if(event->key() == Qt::Key_F2)
        updateTag();
    else if(event->modifiers() == Qt::ControlModifier && event->key() == Qt::Key_S)
        save();
    else if(event->modifiers() == Qt::ControlModifier && event->key() == Qt::Key_Z)
        revert();
}

void TagViewer::on_kw_returnPressed()
{
    QString kw=ui->kw->text().trimmed();
    if(kw.isEmpty())
        return;

    model->setTable(table);
    model->setFilter(QString("detail like '%%1%'").arg(kw));
    model->select();

    ui->tableView->hideColumn(ID_COL);
    model->setHeaderData(1,Qt::Horizontal,QStringLiteral("名称"));    //label
    ui->tableView->setColumnWidth(1,480);
    model->setHeaderData(2,Qt::Horizontal,QStringLiteral("标签"));    //detail
    ui->tableView->hideColumn(3);   //addition
}

void TagViewer::on_tableView_doubleClicked(const QModelIndex &index)
{
    if(isEdit)
        return;

    if(index.isValid())
    {
        QString addition=sp_fetchString(index,3);
        QString detail=sp_getItem("detail",G_LOCATION,"addition",addition).toString();
        if(QFileInfo::exists(detail))
            QDesktopServices::openUrl(QUrl::fromLocalFile(detail));
    }
}
