﻿#include "itemadd.h"
#include "ui_itemadd.h"

ItemAdd::ItemAdd(const QString &table, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ItemAdd)
{
    ui->setupUi(this);

    table_ = table;
}

ItemAdd::~ItemAdd()
{
    delete ui;
}

void ItemAdd::on_btn_clear_clicked()
{
    ui->url->clear();
}

void ItemAdd::on_btn_paste_clicked()
{
    QString url=qApp->clipboard()->text();
    ui->url->setPlainText(url);
}

bool ItemAdd::addItem(const QString &label, const QString &detail, const QString &addition)
{
    QSqlQuery query;
    query.prepare(QString("insert into %1 (label,detail,addition) values (?,?,?)").arg(table_));
    query.addBindValue(label);
    query.addBindValue(detail);
    query.addBindValue(addition);
    return query.exec();
}

void ItemAdd::on_btn_submit_clicked()
{
    QString label=ui->label->text().trimmed();
    if(label.isEmpty())
        return;

    QString url=ui->url->toPlainText();
    if(url.isEmpty())
        return;

    QString addition=ui->addition->text().trimmed();
    if(!addItem(label,url,addition))
        QMessageBox::warning(this,QStringLiteral("异常情况"),QStringLiteral("无法提交数据！"));
    else
        accept();
}
