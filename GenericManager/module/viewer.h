﻿#ifndef VIEWER_H
#define VIEWER_H

#include "common/baseeditor.h"
#include "head/m_message.h"

namespace Ui {
class Viewer;
}

class Viewer : public BaseEditor
{
    Q_OBJECT

public:
    explicit Viewer(QWidget *parent = 0);
    ~Viewer();

    void updateView();
    void updateFolderView();
    void generateMenu();
    void keyPressEvent(QKeyEvent *event);

    void openItemLocation();
    void setResLocation();
    void openResLocation();
    void loadResLocation();
    void saveResLocation();
    void updateResource();  //更新资源
    void scanRescource();   //扫描资源
    void updateTag();  //更新标签

private slots:
    void on_tableView_doubleClicked(const QModelIndex &);
    void on_kw_returnPressed();
    void on_listView_clicked(const QModelIndex &index);
    void on_tableView_clicked(const QModelIndex &index);

private:
    Ui::Viewer *ui;

    QString selectFolder;
    QString selectLabel,selectDetail,selectAddition;
    QString resLocation;
    QStringListModel *listModel;

    bool isNewTag;
    QString tagTable;
};

#endif // VIEWER_H
