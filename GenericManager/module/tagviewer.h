﻿#ifndef TAGVIEWER_H
#define TAGVIEWER_H

#include "common/baseeditor.h"

namespace Ui {
class TagViewer;
}

class TagViewer : public BaseEditor
{
    Q_OBJECT

public:
    explicit TagViewer(QWidget *parent = 0);
    ~TagViewer();

    void updateView();
    void generateMenu();
    void keyPressEvent(QKeyEvent *event);
    void removeItem();
    void updateTag();

private slots:
    void on_kw_returnPressed();

    void on_tableView_doubleClicked(const QModelIndex &index);

private:
    Ui::TagViewer *ui;
};

#endif // TAGVIEWER_H
