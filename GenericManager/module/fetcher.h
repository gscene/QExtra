﻿#ifndef FETCHER_H
#define FETCHER_H

#include "common/baseeditor.h"
#include "itemadd.h"

namespace Ui {
class Fetcher;
}

class Fetcher : public BaseEditor
{
    Q_OBJECT

public:
    explicit Fetcher(QWidget *parent = 0);
    ~Fetcher();

    void updateView();
    void generateMenu();
    void addItem();
    void keyPressEvent(QKeyEvent *event);

private slots:
    void on_tableView_doubleClicked(const QModelIndex &index);
    void on_kw_returnPressed();

private:
    Ui::Fetcher *ui;
};

#endif // FETCHER_H
