﻿#ifndef M_FHS_H
#define M_FHS_H

#include <head/m_head.h>

#define RES_ACG "res_acg_fetch"
#define RES_CONFIG "../etc/resource.ini"

#define ID_COL 0
#define CAT_COL 1
#define LAB_COL 2
#define DET_COL 3
#define ADD_COL 4
#define REF_COL 5

#endif // M_FHS_H
