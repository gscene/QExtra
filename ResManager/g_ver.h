﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "ResManager"
#define SP_VER 1002
#define SP_UID "{fb323c4b-37f6-4458-bc70-0c8242e57548}"
#define SP_TYPE "shell"

#define SP_CFG "ResManager.ini"
#define SP_INFO "ResManager.json"
#define SP_LINK "ResManager.lnk"

/*
*  1002 项目开始
*/
#endif // G_VER_H
