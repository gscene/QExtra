﻿#include "fetcher.h"
#include "ui_fetcher.h"

Fetcher::Fetcher(QWidget *parent) :
    BaseEditor(parent),
    ui(new Ui::Fetcher)
{
    ui->setupUi(this);

    table=RES_ACG;
    listModel=new QStringListModel(this);
    ui->listView->setModel(listModel);
    model=new QSqlTableModel(this);
    ui->tableView->setModel(model);

    updateCategoryView();
    createMenu();
}

Fetcher::~Fetcher()
{
    delete ui;
}

void Fetcher::generateMenu()
{
    menu->addAction(QStringLiteral("添加... (F1)"),this,&Fetcher::newItem);
    menu->addSeparator();
    QMenu *m_update=menu->addMenu(QStringLiteral("更新"));
    m_update->addAction(QStringLiteral("更新列表 (F4)"),this,&Fetcher::updateCategoryView);
    m_update->addAction(QStringLiteral("更新视图 (F5)"),this,&Fetcher::updateView);
    menu->addSeparator();
    menu->addAction(QStringLiteral("打开引用链接 (Alt+W)"),this,&Fetcher::openRef);
    menu->addAction(QStringLiteral("打开资源链接 (Ctrl+R)"),this,&Fetcher::openDetail);
    menu->addAction(QStringLiteral("复制密码 (Ctrl+C)"),this,&Fetcher::copyAddition);
    QMenu *m_opt=menu->addMenu(QStringLiteral("选项"));
    m_opt->addAction(QStringLiteral("删除 (Delete)"),this,&Fetcher::removeItem);
}

void Fetcher::updateCategoryView()
{
    if(listModel->rowCount() != 0)
        listModel->setStringList(QStringList());

    QStringList items=sp_getCategory(table);
    if(!items.isEmpty())
        listModel->setStringList(items);
}

void Fetcher::on_listView_doubleClicked(const QModelIndex &index)
{
    if(index.isValid())
    {
        selectCategory=index.data().toString();
        updateView();
    }
}

void Fetcher::updateView()
{
    if(selectCategory.isEmpty())
        return;

    model->setTable(table);
    model->setFilter(QString("category='%1'").arg(selectCategory));
    model->select();

    ui->tableView->hideColumn(ID_COL);
    ui->tableView->hideColumn(CAT_COL);
    model->setHeaderData(LAB_COL,Qt::Horizontal,QStringLiteral("标题"));
    ui->tableView->hideColumn(DET_COL);
    ui->tableView->hideColumn(ADD_COL);
    ui->tableView->hideColumn(REF_COL);
}

void Fetcher::openDetail()
{
    QModelIndex index=ui->tableView->currentIndex();
    if(index.isValid())
    {
        QString detail=sp_fetchString(index,DET_COL);
        if(detail.isEmpty())
            return;

        QDesktopServices::openUrl(QUrl(detail));
    }
}

void Fetcher::openRef()
{
    QModelIndex index=ui->tableView->currentIndex();
    if(index.isValid())
    {
        QString detail=sp_fetchString(index,REF_COL);
        if(detail.isEmpty())
            return;

        QDesktopServices::openUrl(QUrl(detail));
    }
}

void Fetcher::copyAddition()
{
    QModelIndex index=ui->tableView->currentIndex();
    if(index.isValid())
    {
        QString detail=sp_fetchString(index,ADD_COL);
        if(detail.isEmpty())
            return;

        qApp->clipboard()->setText(detail);
    }
}

void Fetcher::newItem()
{
    AddItem item(this);
    if(!selectCategory.isEmpty())
        item.setCategory(selectCategory);

    if(item.exec() == QDialog::Accepted)
        updateView();
}

void Fetcher::removeItem()
{
    QModelIndex index=ui->tableView->currentIndex();
    if(index.isValid())
    {
        QMessageBox::StandardButton result=MESSAGE_DELETE_CONFIRM
                if(result == QMessageBox::Yes)
        {
                if(sp_removeModelIndex(table,index))
                updateView();
                else
                MESSAGE_CANNOT_DELETE
    }
    }
}

void Fetcher::keyPressEvent(QKeyEvent *event)
{
    if(event->modifiers() == Qt::AltModifier && event->key() == Qt::Key_W)
        openRef();
    else if(event->modifiers() == Qt::ControlModifier && event->key() == Qt::Key_R)
        openDetail();
    else if(event->modifiers() == Qt::ControlModifier && event->key() == Qt::Key_C)
        copyAddition();
    else
    {
        switch (event->key()) {
        case Qt::Key_F1:
            newItem();
            break;
        case Qt::Key_F4:
            updateCategoryView();
            break;
        case Qt::Key_F5:
            updateView();
            break;
        case Qt::Key_Delete:
            removeItem();
            break;
        }
    }
}

void Fetcher::on_tableView_doubleClicked(const QModelIndex &index)
{
    if(index.isValid())
    {
        QString detail=sp_fetchString(index,REF_COL);
        if(detail.isEmpty())
            return;

        QDesktopServices::openUrl(QUrl(detail));
    }
}
