﻿#ifndef FETCHER_H
#define FETCHER_H

#include <common/baseeditor.h>
#include <head/m_message.h>
#include "additem.h"

namespace Ui {
class Fetcher;
}

class Fetcher : public BaseEditor
{
    Q_OBJECT

public:
    explicit Fetcher(QWidget *parent = nullptr);
    ~Fetcher();

    void updateCategoryView();
    void updateView();
    void generateMenu();
    void newItem();
    void removeItem();

    void openDetail();  //打开链接
    void copyAddition();    //复制密码
    void openRef();     //打开来源网址
    void keyPressEvent(QKeyEvent *event);

private slots:
    void on_listView_doubleClicked(const QModelIndex &index);

    void on_tableView_doubleClicked(const QModelIndex &index);

private:
    Ui::Fetcher *ui;

    QStringListModel *listModel;
    QString selectCategory;
};

#endif // FETCHER_H
