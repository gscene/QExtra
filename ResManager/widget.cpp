﻿#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    fetcher=new Fetcher(this);
    ui->tabWidget->addTab(fetcher,QStringLiteral("下载"));
}

Widget::~Widget()
{
    delete ui;
}
