#-------------------------------------------------
#
# Project created by QtCreator 2018-08-13T20:07:28
#
#-------------------------------------------------

QT       += core network sql gui widgets

CONFIG += c++11
TARGET = ResManager
TEMPLATE = app

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj
INCLUDEPATH += module

DEFINES += QT_DEPRECATED_WARNINGS

CONFIG += c++11

SOURCES += \
        main.cpp \
        widget.cpp \
    module/fetcher.cpp \
    module/additem.cpp \
    ../../elfproj/support/sp_env.cpp

HEADERS += \
        widget.h \
    module/fetcher.h \
    g_ver.h \
    m_fhs.h \
    module/additem.h \
    ../../elfproj/common/baseeditor.h \
    ../../elfproj/support/sp_env.h

FORMS += \
        widget.ui \
    module/fetcher.ui \
    module/additem.ui

RESOURCES += \
    res.qrc

RC_ICONS = Paint.ico
