﻿#ifndef ITEMADD_H
#define ITEMADD_H

#include "support/sp_env.h"
#include "m_fhs.h"

namespace Ui {
class ItemAdd;
}

class ItemAdd : public QDialog
{
    Q_OBJECT

public:
    explicit ItemAdd(QWidget *parent,const QString &table);
    ~ItemAdd();

    bool addItem(int week, const QTime &time,
                 const QString &actor,
                 int action,
                 const QString &extra);

private slots:
    void on_btn_submit_clicked();
    void on_btn_pick_clicked();
    void on_week_valueChanged(int value);
    void on_action_currentTextChanged(const QString &text);

private:
    Ui::ItemAdd *ui;

    QTime baseTime;
    QString _table;
    QMap<QString,int> items;
};

#endif // ITEMADD_H
