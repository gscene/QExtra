﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "ScheduleManager"
#define SP_VER 1004
#define SP_UID "{816e2ce8-71e8-458b-8d0d-208607a79152}"
#define SP_TYPE "support"

#define SP_CFG "ScheduleManager.ini"
#define SP_INFO "ScheduleManager.json"
#define SP_LINK "ScheduleManager.lnk"

/*
 * 1002 同步指令
 * 1003 level识别
 * 1004 m_function
 */

#endif // G_VER_H
