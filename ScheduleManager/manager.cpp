﻿#include "manager.h"
#include "ui_manager.h"

Manager::Manager(QWidget *parent) :
    BaseEditorDialog(parent),
    ui(new Ui::Manager)
{
    ui->setupUi(this);

    table=TD_SCHEDULE;
    model=new QSqlTableModel(this);
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    ui->tableView->setModel(model);
    ui->tableView->setEditTriggers(QTableView::DoubleClicked);

    form=0;
    updateView();
    createMenu();
    invokeScheduler();
}

Manager::~Manager()
{
    delete ui;
}

void Manager::updateView()
{
    switch (form) {
    case 0:
        updateView_User();
        break;
    case 1:
        updateView_Group();
        break;
    case 2:
        updateView_All();
        break;
    }
}

void Manager::updateView_User()
{
    form=0;
    model->setTable(table);
    model->setFilter(QString("actor='%1'").arg(user.name));
    model->select();

    setHeaderData();
}

void Manager::updateView_Group()
{
    form=1;
    model->setTable(table);
    model->setFilter(QString("actor='%1' OR actor='%2'").arg(user.name)
                     .arg(user.group));
    model->select();

    setHeaderData();
}

void Manager::updateView_All()
{
    form=2;
    model->setTable(table);
    model->select();

    setHeaderData();
}

void Manager::setHeaderData()
{
    ui->tableView->hideColumn(0);   //uid
    model->setHeaderData(1,Qt::Horizontal,QStringLiteral("日期"));
    model->setHeaderData(2,Qt::Horizontal,QStringLiteral("时间"));
    model->setHeaderData(3,Qt::Horizontal,QStringLiteral("执行者"));
    model->setHeaderData(4,Qt::Horizontal,QStringLiteral("行为"));
    model->setHeaderData(5,Qt::Horizontal,QStringLiteral("内容"));
}

void Manager::generateMenu()
{
    menu->addAction(QStringLiteral("添加..."),this,&Manager::newItem);
    menu->addAction(QStringLiteral("更新"),this,&Manager::updateView_User);
    if(user.level > USER_LEVEL_USER)
        menu->addAction(QStringLiteral("显示当前组"),this,&Manager::updateView_Group);
    if(user.level > USER_LEVEL_GRP)
        menu->addAction(QStringLiteral("显示所有"),this,&Manager::updateView_All);
    menu->addSeparator();
    menu->addAction(QStringLiteral("【同步】"),this,&Manager::sync);
    menu->addSeparator();
    menu->addAction(QStringLiteral("撤销"),this,&Manager::revert);
    menu->addAction(QStringLiteral("保存"),this,&Manager::save);
    menu->addSeparator();
    menu->addAction(QStringLiteral("删除"),this,&Manager::removeItem);
}

void Manager::removeItem()
{
    if(ui->tableView->currentIndex().isValid())
    {
        int curRow = ui->tableView->currentIndex().row();
        model->removeRow(curRow);
    }
}

void Manager::newItem()
{
    ItemAdd itemAdd(this,TD_SCHEDULE);
    if(itemAdd.exec() == QDialog::Accepted)
        updateView();
}

void Manager::sync()
{
    ::sp_localSend(R_CMD_SYNC,PORT_SCHEDULER);
    QMessageBox::information(this,QStringLiteral("指令已发送"),
                             QStringLiteral("同步指令已发送"));
}

void Manager::invokeScheduler()
{
    if(QFile::exists(SCHEDULER))
        QDesktopServices::openUrl(QUrl(SCHEDULER));
}
