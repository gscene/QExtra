﻿#ifndef MANAGER_H
#define MANAGER_H

#include "common/baseeditor_dialog.h"
#include "head/g_functionbase.h"
#include "common/user.h"

#include "support/ref_commands.h"
#include "itemadd.h"

#ifdef Q_OS_WIN
#define SCHEDULER "Scheduler.exe"
#else
#define SCHEDULER "Scheduler"
#endif

namespace Ui {
class Manager;
}

class Manager : public BaseEditorDialog
{
    Q_OBJECT

public:
    explicit Manager(QWidget *parent = 0);
    ~Manager();

    void updateView();
    void updateView_User();
    void updateView_Group();
    void updateView_All();
    void setHeaderData();
    void generateMenu();
    void removeItem();
    void newItem();
    void sync();
    void invokeScheduler();

private:
    Ui::Manager *ui;

    int form;
    User user;
};

#endif // MANAGER_H
