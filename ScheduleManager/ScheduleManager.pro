#-------------------------------------------------
#
# Project created by QtCreator 2017-03-29T09:55:27
#
#-------------------------------------------------

QT       += core gui sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = ScheduleManager
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj

SOURCES += main.cpp\
        manager.cpp \
    itemadd.cpp \
    ../../elfproj/support/sp_env.cpp

HEADERS  += manager.h \
    itemadd.h \
    g_ver.h \
    ../../elfproj/common/baseeditor_dialog.h \
    ../common/m_fhs.h \
    ../../elfproj/support/sp_env.h \
    ../support/ref_commands.h \
    ../../elfproj/common/user.h

FORMS    += manager.ui \
    itemadd.ui

RC_ICONS = scheduler.ico

RESOURCES += \
    res.qrc
