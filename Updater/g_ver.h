﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "Updater"
#define SP_VER 1001
#define SP_UID "{9f80bf8c-7e49-40fe-bf18-7258a7eeab76}"
#define SP_TYPE "support"

#define SP_CFG "Updater.ini"
#define SP_INFO "Updater.json"
#define SP_LINK "Updater.lnk"

#endif // G_VER_H
