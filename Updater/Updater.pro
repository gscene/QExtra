#-------------------------------------------------
#
# Project created by QtCreator 2017-04-09T20:31:37
#
#-------------------------------------------------

QT       += core gui sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = Updater
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS
include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj
INCLUDEPATH += ../common

SOURCES += main.cpp\
        updater.cpp \
    ../../elfproj/support/sp_env.cpp \
    ../../elfproj/support/downloader.cpp

HEADERS  += updater.h \
    g_ver.h \
    ../common/m_fhs.h \
    ../../elfproj/support/sp_env.h \
    ../../elfproj/support/downloader.h
