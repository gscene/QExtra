﻿#ifndef UPDATER_H
#define UPDATER_H

#include "support/sp_env.h"
#include "head/g_functionbase.h"
#include "head/m_function.h"
#include "head/g_fhs.h"
#include "support/downloader.h"

class Updater : public QWidget
{
    Q_OBJECT

public:
    Updater(QWidget *parent = 0);
    ~Updater();

    void checkSpace();
    void checkEntry(const QJsonObject &obj);
    void startDownloader();
    bool operate_verify(const QString &filePath, const QString &sha1);
    void operate_move();

    void createStorage();
    bool db_try();
    void downloadFinished(const QString &url);

signals:
    void downloadUrls(const QStringList &urls);

private:
    QSqlDatabase db;
    QStringList downloadFiles;
    QStringList updateFiles;

    Downloader *downloader;
    QThread downloadThread;
};

#endif // UPDATER_H
