﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "DeveloperAide"
#define SP_VER 1011
#define SP_UID "{18a34a82-dea1-4ff7-a342-90bd7b3c07bb}"
#define SP_TYPE "extra"

#define SP_CFG "DeveloperAide.ini"
#define SP_INFO "DeveloperAide.json"
#define SP_LINK "DeveloperAide.lnk"

/*
 * 1001 项目开始
 * 1002 采用子线程
 * 1003 取消加载外部文件
 * 1004 目录历史
 * 1005 路径转换
 * 1006 外部文件调用
 * 1007 增加打开目录
 * 1008 重写定时器功能
 * 1009 使用配置文件设置自动开启监控
 * 1010 #15
 * 1011 #18
*/
#endif // G_VER_H
