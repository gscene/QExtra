﻿#ifndef FILEMONITOR_H
#define FILEMONITOR_H

#include <QObject>
#include <QTimer>
#include <QStringList>
#include <QHash>
#include <QThread>
#include <QSqlQuery>
#include <QVariant>

#include "m_fhs.h"
#include "head/g_functionbase.h"

class FileMonitor : public QObject
{
    Q_OBJECT

public:
    explicit FileMonitor(QObject *parent = 0);

    void generateTarget();
    void updateTarget();

signals:
    void monitoring(bool flag);
    void fileChanged(const QStringList &fileList);

public slots:
    void startMonitor(bool flag);

private:
    QTimer *timer;
    QStringList targetList;
    QStringList activityList;
    QHash<QString,uint> targetTable;
};

#endif // FILEMONITOR_H
