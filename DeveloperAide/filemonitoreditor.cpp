﻿#include "filemonitoreditor.h"
#include "ui_filemonitoreditor.h"

FileMonitorEditor::FileMonitorEditor(QWidget *parent) :
    BaseEditorDialog(parent),
    ui(new Ui::FileMonitorEditor)
{
    ui->setupUi(this);

    model=new QSqlTableModel(this);
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    ui->tableView->setModel(model);

    ui->action->addItems(QStringList() << TEXT_NONE << TEXT_COPY <<TEXT_EXECUTE << TEXT_DELETE);
    items.insert(TEXT_NONE,ACT_NONE);
    items.insert(TEXT_COPY,ACT_COPY);
    items.insert(TEXT_EXECUTE,ACT_EXECUTE);
    items.insert(TEXT_DELETE,ACT_DELETE);
    ui->action->setCurrentIndex(-1);

    ui->btn_extra->setEnabled(false);
    ui->extra->setEnabled(false);

    QSettings set(M_CONFIG,QSettings::IniFormat);
    prefix=set.value(KEY_PREFIX).toString();
    postfix=set.value(KEY_POSTFIX).toString();

    updateView();
    createMenu();
}

FileMonitorEditor::~FileMonitorEditor()
{
    delete ui;
}

void FileMonitorEditor::updateView()
{
    model->setTable(TD_MONITOR);
    model->select();

    ui->tableView->hideColumn(0);   //id
    model->setHeaderData(1,Qt::Horizontal,QStringLiteral("名称"));    //label
    model->setHeaderData(2,Qt::Horizontal,QStringLiteral("位置"));    //detail
    model->setHeaderData(3,Qt::Horizontal,QStringLiteral("行为"));    //action
    model->setHeaderData(4,Qt::Horizontal,QStringLiteral("内容"));    //extra
}

void FileMonitorEditor::createMenu()
{
    menu=new QMenu(this);
    menu->addAction(QStringLiteral("打开目录"),this,&FileMonitorEditor::openDir);
    menu->addAction(QStringLiteral("更新视图"),this,&FileMonitorEditor::updateView);
    menu->addSeparator();
    menu->addAction(QStringLiteral("保存"),this,&FileMonitorEditor::save);
    menu->addAction(QStringLiteral("撤销"),this,&FileMonitorEditor::revert);
    menu->addSeparator();
    menu->addAction(QStringLiteral("删除"),this,&FileMonitorEditor::removeItem);
}

bool FileMonitorEditor::addItem(const QString &label, const QString &detail, int action, const QString &extra)
{
    QSqlQuery query;
    query.prepare(QString("insert into %1 (label,detail,action,extra) values (?,?,?,?)").arg(TD_MONITOR));
    query.addBindValue(label);
    query.addBindValue(detail);
    query.addBindValue(action);
    query.addBindValue(extra);
    if(query.exec())
        return true;
    else
    {
        qDebug() << query.lastError().text();
        return false;
    }
}

void FileMonitorEditor::removeItem()
{
    if(ui->tableView->currentIndex().isValid())
    {
        int curRow = ui->tableView->currentIndex().row();
        model->removeRow(curRow);
    }
}

void FileMonitorEditor::openDir()
{
    QModelIndex index=ui->tableView->currentIndex();
    if(index.isValid())
    {
        QSqlRecord record=model->record(index.row());
        QString item=record.value("detail").toString();
        sp_openDirByFile(item);
    }
}


void FileMonitorEditor::on_btn_pick_clicked()
{
    QString location=QFileDialog::getExistingDirectory(this,QStringLiteral("选择目录"),getLocation());
    if(location.isEmpty())
        return;

    QString name=ui->name->text().trimmed();
    if(name.isEmpty())
    {
        QDir dir(location);
        name=dir.dirName();
        ui->name->setText(name);
    }

    putLocation(location);
    locationTransform(location);
    QString path=location;

#ifdef Q_OS_WIN
    path=location + "/" + name +QString(".exe");
#else
    path=location + "/" + name;
#endif
    ui->path->setText(path);
}

void FileMonitorEditor::locationTransform(QString &location)
{
    QString dirName=QDir(location).dirName();
    if(dirName != "release")
    {
        location.remove(dirName);
        dirName = prefix + dirName + postfix;
        location += dirName;
    }
}

void FileMonitorEditor::putLocation(const QString &location)
{
    QSettings cfg(M_CONFIG,QSettings::IniFormat);
    cfg.setValue(KEY_TRAIL,location);
}

QString FileMonitorEditor::getLocation() const
{
    QSettings cfg(M_CONFIG,QSettings::IniFormat);
    return cfg.value(KEY_TRAIL).toString();
}

void FileMonitorEditor::on_btn_sure_clicked()
{
   accept();
}

void FileMonitorEditor::on_btn_extra_clicked()
{

}

void FileMonitorEditor::on_action_currentTextChanged(const QString &text)
{
    if(text == TEXT_COPY || text == TEXT_EXECUTE)
    {
        ui->btn_extra->setEnabled(true);
        ui->extra->setEnabled(true);
    }
    else
    {
        ui->btn_extra->setEnabled(false);
        ui->extra->setEnabled(false);
    }
}

void FileMonitorEditor::on_btn_submit_clicked()
{
    QString name=ui->name->text().trimmed();
    QString path=ui->path->text().trimmed();
    if(name.isEmpty() || path.isEmpty())
        return;

    int action=0;
    if(ui->action->currentIndex() != -1)
    {
        QString key=ui->action->currentText();
        if(items.contains(key))
            action=items.value(key);
    }

    QString extra{};
    if(ui->extra->isEnabled())
    {
        extra=ui->extra->text().trimmed();
        if(extra.isEmpty())
            return;
    }

    if(addItem(name,path,action,extra))
    {
        updateView();
        ui->name->clear();
        ui->path->clear();
    }
    else
        QMessageBox::warning(this,QStringLiteral("异常情况"),QStringLiteral("无法提交数据！"));
}
