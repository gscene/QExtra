﻿#include "aide.h"

Aide::Aide(QWidget *parent)
    : QWidget(parent)
{

    isMonitoring = false;
    createMenu();
    createTray();

    startFileMonitorThread();

    QSettings set(M_CONFIG,QSettings::IniFormat);
    if(set.value(KEY_ENABLE).toInt() == 1)
        emit startMonitor(true);
}

Aide::~Aide()
{
    monitorThread.quit();
    monitorThread.wait();
}

void Aide::startFileMonitorThread()
{
    fileMonitor=new FileMonitor;            // 被移动的对象不能有父对象！
    connect(&monitorThread,&QThread::finished,fileMonitor,&QObject::deleteLater);
    connect(this,&Aide::startMonitor,fileMonitor,&FileMonitor::startMonitor);
    connect(fileMonitor,&FileMonitor::monitoring,this,&Aide::monitoring);
    connect(fileMonitor,&FileMonitor::fileChanged,this,&Aide::fileChanged);

    fileMonitor->moveToThread(&monitorThread);
    monitorThread.start();
}

void Aide::startFileMonitor()
{
    emit startMonitor(true);
}

void Aide::stopFileMonitor()
{
    emit startMonitor(false);
}

void Aide::monitoring(bool flag)
{
    isMonitoring = flag;
    if(isMonitoring)
        tray->showMessage(QStringLiteral("监视中"),QStringLiteral("文件监视开始执行"));
    else
        tray->showMessage(QStringLiteral("已停止"),QStringLiteral("文件监视已停止"));
    updateMenu();
}

void Aide::createTray()
{
    tray=new QSystemTrayIcon(this);
    tray->setIcon(QIcon(":/aide.ico"));
    tray->setContextMenu(tray_Menu);
    tray->setToolTip(QStringLiteral("Aide就绪"));
    tray->show();
}

void Aide::createMenu()
{
    tray_Menu=new QMenu(this);

    QMenu *m_fileMonitor=tray_Menu->addMenu(QStringLiteral("文件监控"));
    m_fileMonitor->addAction(QStringLiteral("编辑..."),this,&Aide::showFileMonitor);
    if(isMonitoring)
        m_fileMonitor->addAction(QStringLiteral("停止监控"),this,&Aide::stopFileMonitor);
    else
        m_fileMonitor->addAction(QStringLiteral("开始监控"),this,&Aide::startFileMonitor);
    tray_Menu->addSeparator();
    tray_Menu->addAction(QStringLiteral("退出"),QApplication::quit);
}

void Aide::updateMenu()
{
    tray_Menu->clear();

    QMenu *m_fileMonitor=tray_Menu->addMenu(QStringLiteral("文件监控"));
    m_fileMonitor->addAction(QStringLiteral("编辑..."),this,&Aide::showFileMonitor);
    if(isMonitoring)
        m_fileMonitor->addAction(QStringLiteral("停止监控"),this,&Aide::stopFileMonitor);
    else
        m_fileMonitor->addAction(QStringLiteral("开始监控"),this,&Aide::startFileMonitor);
    tray_Menu->addSeparator();
    tray_Menu->addAction(QStringLiteral("退出"),QApplication::quit);
}

void Aide::showFileMonitor()
{
    FileMonitorEditor editor;
    if(editor.exec() == QDialog::Accepted)
        emit startMonitor(true);
}

void Aide::fileChanged(const QStringList &fileList)
{
    tray->showMessage(QStringLiteral("文件变化"),QStringLiteral("监测到有文件发生变化，现在开始处理"));
    processFileChanged(fileList);

    /*
    QSettings set(M_CONFIG,QSettings::IniFormat);
    if(set.contains(KEY_FM_SCRIPT))
    {
        QString command=set.value(KEY_FM_SCRIPT).toString();
        if(command.isEmpty())
            return;
        QProcess::startDetached(command);
    }
    */
}

void Aide::processFileChanged(const QStringList &pathList)
{
    /*
    foreach (QString path, pathList) {
        if(QFile::exists(path))
        {
            QString fileName = QFileInfo(path).fileName();
            QString target = destination + "/" + fileName;
            QFile::copy(path,target);
            qInfo() << QStringLiteral("已复制文件：") + fileName;
        }
    }
    */
}
