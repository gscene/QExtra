﻿#include "aide.h"
#include <QtSingleApplication>
#include <QMessageBox>

#include "head/e_boot.h"
#include "support/mysql_helper.h"

int main(int argc, char *argv[])
{
    SP_Single_Boot

    if(!createConnection(DB_CONFIG))
    {
        QMessageBox::critical(0,QStringLiteral("致命错误"),QStringLiteral("无法连接到数据库！"));
        return EXIT_FAILURE;
    }

    a.setQuitOnLastWindowClosed(false);

    Aide w;
    a.setActivationWindow(&w);
    w.hide();

    return a.exec();
}
