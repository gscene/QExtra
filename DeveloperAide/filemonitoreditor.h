﻿#ifndef FILEMONITOREDITOR_H
#define FILEMONITOREDITOR_H

#include "head/g_function.h"
#include "common/baseeditor_dialog.h"

#include "m_fhs.h"

namespace Ui {
class FileMonitorEditor;
}

class FileMonitorEditor : public BaseEditorDialog
{
    Q_OBJECT

public:
    explicit FileMonitorEditor(QWidget *parent = 0);
    ~FileMonitorEditor();

    void updateView();
    void createMenu();
    void openDir();

    bool addItem(const QString &label, const QString &detail,int action,const QString &extra);
    void removeItem();

    QString getLocation() const;
    void putLocation(const QString &location);
    void locationTransform(QString &location);

private slots:
    void on_btn_pick_clicked();
    void on_btn_sure_clicked();
    void on_btn_extra_clicked();
    void on_action_currentTextChanged(const QString &text);
    void on_btn_submit_clicked();

private:
    Ui::FileMonitorEditor *ui;
    QMap<QString,int> items;

    QString prefix,postfix;
};

#endif // FILEMONITOREDITOR_H
