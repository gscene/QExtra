﻿#ifndef M_FHS_H
#define M_FHS_H

#include "g_ver.h"
#include "head/g_fhs.h"

#define APP_ROOT "/QVertex"
#define APP_REG "HKEY_CURRENT_USER\\Software\\elfproj\\QVertex"
#define M_CONFIG QString(P_FHS_ETC) + SP_CFG
#define DB_CONFIG "../etc/local.ini"

#define TD_MONITOR "file_monitor"

#define KEY_PREFIX "FileMonitor/prefix"
#define KEY_POSTFIX "FileMonitor/postfix"
#define KEY_TRAIL "FileMonitor/trail"     //目录历史痕迹
#define KEY_ENABLE "FileMonitor/enable"        //自动开始监控

#define TEXT_NONE "None"
#define TEXT_COPY "Copy"
#define TEXT_EXECUTE "Execute"
#define TEXT_DELETE "Delete"

#define ACT_NONE 0
#define ACT_COPY 1
#define ACT_EXECUTE 2
#define ACT_DELETE 9

#endif // M_FHS_H
