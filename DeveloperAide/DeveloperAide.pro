#-------------------------------------------------
#
# Project created by QtCreator 2016-09-22T19:05:12
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = DeveloperAide
TEMPLATE = app

include(../qtsingleapplication/src/qtsingleapplication.pri)

INCLUDEPATH += ../../elfproj

SOURCES += main.cpp\
        aide.cpp \
    filemonitoreditor.cpp \
    filemonitor.cpp \
    ../../elfproj/common/baseeditordialog.cpp \
    ../../elfproj/support/sp_env.cpp

HEADERS  += aide.h \
    g_ver.h \
    filemonitoreditor.h \
    filemonitor.h \
    m_fhs.h \
    ../../elfproj/common/baseeditordialog.h \
    ../../elfproj/support/sp_env.h

RESOURCES += \
    res.qrc

FORMS += \
    filemonitoreditor.ui

RC_ICONS = aide.ico
