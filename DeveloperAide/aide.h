﻿#ifndef AIDE_H
#define AIDE_H

#include <QWidget>
#include <QApplication>
#include <QSettings>
#include <QSystemTrayIcon>
#include <QIcon>
#include <QMenu>
#include <QAction>
#include <QFileInfo>
#include <QProcess>
#include <QDebug>

#include <filemonitor.h>
#include <filemonitoreditor.h>

class Aide : public QWidget
{
    Q_OBJECT

public:
    explicit Aide(QWidget *parent = 0);
    ~Aide();

    void createTray();
    void createMenu();
    void updateMenu();

    void startFileMonitorThread();

    void startFileMonitor();
    void stopFileMonitor();
    void fileChanged(const QStringList &fileList);
    void processFileChanged(const QStringList &pathList);

    void monitoring(bool flag);
    void showFileMonitor();

signals:
    void startMonitor(bool flag);

private:
    QSystemTrayIcon *tray;

    bool isMonitoring;

    QMenu *tray_Menu;

    FileMonitor *fileMonitor;
    QThread monitorThread;
};

#endif // AIDE_H
