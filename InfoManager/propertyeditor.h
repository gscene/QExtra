﻿#ifndef PROPERTYEDITOR_H
#define PROPERTYEDITOR_H

#include "common/replyeditordialog.h"

#define LABEL_T "label"
#define DETAIL_T "properties"
#define CATEGORY_T "category"

namespace Ui {
class PropertyEditor;
}

class PropertyEditor : public ReplyEditorDialog
{
    Q_OBJECT

public:
    explicit PropertyEditor(QWidget *parent = 0);
    ~PropertyEditor();

    void replyFinished(QNetworkReply *reply);

private slots:
        void on_btn_add_clicked();
        void on_btn_del_clicked();
        void on_btn_save_clicked();
        void on_categoryList_doubleClicked(const QModelIndex &index);

private:
    Ui::PropertyEditor *ui;
};

#endif // PROPERTYEDITOR_H
