﻿#include "manager.h"
#include "ui_manager.h"

Manager::Manager(QWidget *parent) :
    ReplyEditor(parent),
    ui(new Ui::Manager)
{
    ui->setupUi(this);

    ui->categoryList->setModel(categoryModel);
    ui->listView->setModel(itemModel);

    if(!init())
        QMessageBox::critical(this,QStringLiteral("配置文件缺失"),QStringLiteral("配置文件缺失！"));
}

Manager::~Manager()
{
    delete ui;
}

void Manager::edit_category()
{

}

void Manager::replyFinished(QNetworkReply *reply)
{
    QByteArray data=reply->readAll();
    Pie pie(data);
    if(pie.get("uid") == T_UID)
    {
        offline=false;
        QString url=base_url + "category_list";
        httpGet(url);
    }
    else if(pie.contain(DETAIL_T))
    {
        Piev piev(data,DETAIL_T);
        properties=piev.getStringList();
        itemModel->setStringList(properties);
    }
    else if(pie.contain("categories"))
    {
        Piev piev(data,"categories");
        QStringList categories=piev.getStringList();
        categoryModel->setStringList(categories);
    }
    else if(pie.contain(current_category))
    {
        Piev piev(data,current_category);
        QStringList items=piev.getStringList();
        itemModel->setStringList(items);
    }
    reply->deleteLater();
}

void Manager::on_categoryList_doubleClicked(const QModelIndex &index)
{
    if(index.isValid())
    {
        itemModel->setStringList(QStringList());
        current_category=categoryModel->data(index,Qt::DisplayRole).toString();

        QJsonObject selector;
        selector.insert(LABEL_T,current_category);
        selector.insert(CATEGORY_T,"_meta_");

        QString url=base_url + "category/" + current_category;
        httpGet(url);
    }
}

void Manager::on_listView_doubleClicked(const QModelIndex &index)
{
    if(current_category.isEmpty())
        return;

    if(index.isValid())
    {
        current_item=itemModel->data(index,Qt::DisplayRole).toString();
        ItemEditor editor(this,current_category,current_item);
        editor.exec();
    }
}
