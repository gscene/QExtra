﻿#ifndef MANAGER_H
#define MANAGER_H

#include "itemeditor.h"
#include "propertyeditor.h"
#include "common/replyeditor.h"

namespace Ui {
class Manager;
}

class Manager : public ReplyEditor
{
    Q_OBJECT

public:
    explicit Manager(QWidget *parent = 0);
    ~Manager();

    void replyFinished(QNetworkReply *reply);

    void edit_category();

private slots:
    void on_categoryList_doubleClicked(const QModelIndex &index);

    void on_listView_doubleClicked(const QModelIndex &index);

private:
    Ui::Manager *ui;

    QStringList properties;
};

#endif // MANAGER_H
