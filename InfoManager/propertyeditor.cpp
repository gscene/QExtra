﻿#include "propertyeditor.h"
#include "ui_propertyeditor.h"

PropertyEditor::PropertyEditor(QWidget *parent) :
    ReplyEditorDialog(parent),
    ui(new Ui::PropertyEditor)
{
    ui->setupUi(this);

    ui->categoryList->setModel(categoryModel);
    ui->listView->setModel(itemModel);
}

PropertyEditor::~PropertyEditor()
{
    delete ui;
}

void PropertyEditor::replyFinished(QNetworkReply *reply)
{
    QByteArray data=reply->readAll();

    Pie pie(data);
    if(pie.contain("uid") && pie.get("uid") == T_UID)
        offline=false;
    else if(pie.contain(DETAIL_T))
    {
        Piev piev(data,DETAIL_T);
        ui->in_category->setText(piev.get(LABEL_T));
        current_category=piev.get(LABEL_T);

        QStringList properties=piev.getStringList();
        itemModel->setStringList(properties);
    }
    else if(pie.contain("categories"))
    {
        Piev piev(data,"categories");
        QStringList categories=piev.getStringList();
        categoryModel->setStringList(categories);
    }
}

void PropertyEditor::on_btn_add_clicked()
{
    QString category=ui->in_category->text().trimmed();
    if(category.isEmpty())
    {
        QMessageBox::warning(this,QStringLiteral("不能为空"),QStringLiteral("类别不能为空！"));
        return;
    }

    QString prop=ui->in_prop->text().trimmed();
    if(prop.isEmpty())
        return;

    if(category !=current_category)
    {
        current_category=category;
        itemModel->setStringList(QStringList());
    }

    int row=itemModel->rowCount();
    itemModel->insertRow(row);
    QModelIndex index=itemModel->index(row);
    itemModel->setData(index,prop);
    ui->in_prop->clear();
}

void PropertyEditor::on_btn_del_clicked()
{
    int row=ui->listView->currentIndex().row();
    itemModel->removeRow(row);
}

void PropertyEditor::on_btn_save_clicked()
{
    QString category=ui->in_category->text().trimmed();
    if(category.isEmpty())
    {
        QMessageBox::warning(this,QStringLiteral("不能为空"),QStringLiteral("类别不能为空！"));
        return;
    }

    if(offline)
    {
        QMessageBox::critical(this,QStringLiteral("无法操作"),QStringLiteral("无法连接到服务器，无法操作！"));
        return;
    }
    else
    {
        if(category !=current_category)
        {
            current_category=category;

            QJsonObject obj;
            obj.insert(LABEL_T,category);
            obj.insert(CATEGORY_T,"_meta_");
            obj.insert(DETAIL_T,QJsonArray::fromStringList(itemModel->stringList()));

            QString url=base_url + "put";

            QJsonDocument doc(obj);

            httpPost(url,doc.toJson());
        }
        else
        {
            QJsonObject selector;
            selector.insert(LABEL_T,category);
            selector.insert(CATEGORY_T,"_meta_");

            QJsonObject updater;
            updater.insert(DETAIL_T,QJsonArray::fromStringList(itemModel->stringList()));

            QJsonArray array;
            array.append(selector);
            array.append(updater);

            QString url=base_url + "update";
            QJsonDocument doc(array);
            httpPost(url,doc.toJson());
        }
    }
}

void PropertyEditor::on_categoryList_doubleClicked(const QModelIndex &index)
{
    if(index.isValid())
    {
        itemModel->setStringList(QStringList());
        current_item=categoryModel->data(index,Qt::DisplayRole).toString();

        QJsonObject selector;
        selector.insert(LABEL_T,current_item);
        selector.insert(CATEGORY_T,"_meta_");

        QString url=base_url + "get_one";
        QJsonDocument doc(selector);
        httpPost(url,doc.toJson());
    }
}
