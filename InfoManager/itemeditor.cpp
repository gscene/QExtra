﻿#include "itemeditor.h"
#include "ui_itemeditor.h"

ItemEditor::ItemEditor(QWidget *parent, const QString &category) :
    ReplyEditorDialog(parent),
    _category(category),
    ui(new Ui::ItemEditor)
{
    ui->setupUi(this);

    if(!init())
        QMessageBox::critical(this,QStringLiteral("配置文件缺失"),QStringLiteral("配置文件缺失！"));
    else
    {
        QString url=base_url + "properties/" + _category;
        httpGet(url);
    }
}

ItemEditor::ItemEditor(QWidget *parent, const QString &category, const QString &label) :
    ReplyEditorDialog(parent),
    _category(category),
    _label(label),
    ui(new Ui::ItemEditor)
{
    ui->setupUi(this);

    if(!init())
        QMessageBox::critical(this,QStringLiteral("配置文件缺失"),QStringLiteral("配置文件缺失！"));
    else
    {
        QString url=base_url + "find/" + _category + QString("/") + _label;
        httpGet(url);
    }
}

ItemEditor::ItemEditor(QWidget *parent, const QJsonObject &object)
    : ReplyEditorDialog(parent),
      _obj(object)
{
    ui->setupUi(this);
}

ItemEditor::~ItemEditor()
{
    delete ui;
}

void ItemEditor::replyFinished(QNetworkReply *reply)
{
    QByteArray data=reply->readAll();
    Pie pie(data);
    if(pie.get("category") == _category &&
            pie.get("label") == _label)
    {
        _obj=QJsonDocument::fromJson(data).object();
        QString url=base_url + "properties/" + _category;
        httpGet(url);
    }
    else if(pie.contain("properties") &&
            pie.get("label") == _category)
    {
        Piev piev(data,"properties");
        _properties=piev.getStringList();

        if(_label.isEmpty())
            setEmptyWidget();
        else
            setJsonWidget();
    }
}

void ItemEditor::setEmptyWidget()
{
    if(_properties.isEmpty())
        return;

    ui->tableWidget->setRowCount(_properties.size());
    ui->tableWidget->setColumnCount(2);

    for(int i=0;i< _properties.size();i++)
    {
        ui->tableWidget->setItem(i,0,new QTableWidgetItem(_properties.at(i)));
    }
}

void ItemEditor::setJsonWidget()
{
    if(_obj.isEmpty())
        return;

    if(_properties.isEmpty())
        return;

    ui->tableWidget->setRowCount(_properties.size());
    ui->tableWidget->setColumnCount(2);

    ui->tableWidget->setItem(0,0,new QTableWidgetItem(QStringLiteral("名称")));
    ui->tableWidget->setItem(0,1,new QTableWidgetItem(_label));

    for(int i=0;i< _properties.size();i++)
    {
        QString key=_properties.at(i);
        QString value=_obj.value(key).toString();

        ui->tableWidget->setItem(i+1,0,new QTableWidgetItem(key));
        ui->tableWidget->setItem(i+1,1,new QTableWidgetItem(value));
    }
}

void ItemEditor::save()
{
    QJsonObject obj;

    for(int i=0;i< ui->tableWidget->rowCount();i++)
    {
        QString key=ui->tableWidget->item(i,0)->data(Qt::DisplayRole).toString();
        QString value=ui->tableWidget->item(i,1)->data(Qt::DisplayRole).toString();
        obj.insert(key,value);
    }
}
