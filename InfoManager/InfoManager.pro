#-------------------------------------------------
#
# Project created by QtCreator 2016-12-30T19:11:55
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = InfoManager
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH += ../../elfproj

SOURCES += main.cpp\
    ../../elfproj/support/piev.cpp \
    ../../elfproj/support/pie.cpp \
    manager.cpp \
    propertyeditor.cpp \
    itemeditor.cpp \
    ../../elfproj/common/replyeditor.cpp \
    ../../elfproj/common/replyeditordialog.cpp

HEADERS  += manager.h \
    ../../elfproj/support/piev.h \
    ../../elfproj/support/pie.h \
    propertyeditor.h \
    itemeditor.h \
    ../../elfproj/common/replyeditor.h \
    ../../elfproj/common/replyeditordialog.h

FORMS    += manager.ui \
    propertyeditor.ui \
    itemeditor.ui
