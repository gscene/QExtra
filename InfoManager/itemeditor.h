﻿#ifndef ITEMEDITOR_H
#define ITEMEDITOR_H

#include <QTabWidget>
#include <QTableWidgetItem>

#include "common/replyeditordialog.h"

namespace Ui {
class ItemEditor;
}

class ItemEditor : public ReplyEditorDialog
{
    Q_OBJECT

public:
    ItemEditor(QWidget *parent,const QString &category);
    ItemEditor(QWidget *parent,const QString &category,
               const QString &label);
    ItemEditor(QWidget *parent, const QJsonObject &object);

    ~ItemEditor();

    void replyFinished(QNetworkReply *reply);
    void setEmptyWidget();
    void setJsonWidget();
    void save();

private:
    Ui::ItemEditor *ui;

    QString _category;
    QString _label;
    QStringList _properties;

    QJsonObject _obj;
};

#endif // ITEMEDITOR_H
