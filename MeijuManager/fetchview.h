﻿#ifndef FETCHVIEW_H
#define FETCHVIEW_H

#include "common/baseeditor_dialog.h"
#include "element/simpleprovider.h"

namespace Ui {
class FetchView;
}

class FetchView : public BaseEditorDialog
{
    Q_OBJECT

public:
    explicit FetchView(QWidget *parent,
                       const QString &label,
                       const QString &detail);
    ~FetchView();

    void generateMenu();
    void updateLocalLocation();     //本地文件列表
    void updateFetchView();     //本地下载源
    void sync();       //更新下载源
    void do_sync();
    void checkSyncTime();
    void updateSyncTime();
    bool timeoutCheck(const QString &time);

    bool addFetchItem(const QString &label,
                 const QString &detail);

    void onReceived(const QByteArray &data);

private slots:
    void on_fetchView_doubleClicked(const QModelIndex &index);

private:
    Ui::FetchView *ui;

    SimpleProvider *provider;

    QString _label;
    QString _detail;
    QString linkTable;
    QString fetchTable;
    QDateTime lastSyncTime;

    QStringListModel *fetchModel;

    QStringList localList;
    QStringList fetchList;
    QMap<QString,QString> fetchDetails;
};

#endif // FETCHVIEW_H
