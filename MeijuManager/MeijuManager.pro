#-------------------------------------------------
#
# Project created by QtCreator 2017-11-11T22:11:23
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MeijuManager
TEMPLATE = app

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        main.cpp \
    widget.cpp \
    fetchview.cpp \
    ../../elfproj/support/sp_env.cpp

HEADERS += \
    widget.h \
    g_ver.h \
    m_fhs.h \
    fetchview.h \
    ../../elfproj/common/baseeditor.h \
    ../../elfproj/element/simpleprovider.h \
    ../../elfproj/support/sp_env.h \
    ../../elfproj/common/baseeditor_dialog.h \
    ../../elfproj/element/locationsynchronizer.h

FORMS += \
        widget.ui \
    fetchview.ui

RESOURCES += \
    res.qrc

RC_ICONS = videos.ico
