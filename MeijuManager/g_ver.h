﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "MeijuManager"
#define SP_VER 1007
#define SP_UID "{3d030e9a-f99f-4823-bcce-a83d4633c0b5}"
#define SP_TYPE "extra"
#define SP_VERSION "[Version " + QString::number(SP_VER) + QString("]")

#define SP_CFG "MeijuManager.ini"
#define SP_INFO "MeijuManager.json"
#define SP_LINK "MeijuManager.lnk"

/*
 * 1002
 * 1003 link表结构调整
 * 1004 Sync
 * 1005 AutoSync
 * 1006 FileSystemWatcher
 * 1007 upProcess,downProcess
*/

#endif // G_VER_H
