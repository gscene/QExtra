﻿#ifndef WIDGET_H
#define WIDGET_H

#include "common/baseeditor.h"
#include "element/locationsynchronizer.h"
#include "fetchview.h"

// Entry 条目

namespace Ui {
class Widget;
}

class Widget : public BaseEditor
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

    void generateMenu();
    void updateEntryView();
    void updateSiteView();
    void updateLocationView(const QString &label);
    void openFetchView();
    void openEntrySite();

    void updateTopicLocation();
    void openTopicLocation();
    void startSynchronizer();
    void stopSynchronizer();

private slots:
    void on_entryView_doubleClicked(const QModelIndex &index);
    void on_siteView_doubleClicked(const QModelIndex &index);
    void on_locationView_doubleClicked(const QModelIndex &index);

private:
    Ui::Widget *ui;

    QString locationTable;
    QMap<QString,QString> topicLinks;
    QMap<QString,QString> topicDetails;
    QMap<QString,QString> itemDetails;
    QMap<QString,QString> topicSites;

    QStringListModel *entryModel;
    QStringListModel *locationModel;
    QStringListModel *siteModel;

    LocationSynchronizer *synchronizer;
    QThread syncThread;
};

#endif // WIDGET_H
