﻿#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    BaseEditor(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    table=T_RES_LINK;
    locationTable=T_RES_LOCATION;
    entryModel=new QStringListModel(this);
    ui->entryView->setModel(entryModel);
    ui->entryView->setEditTriggers(QListView::NoEditTriggers);

    siteModel=new QStringListModel(this);
    ui->siteView->setModel(siteModel);
    ui->siteView->setEditTriggers(QListView::NoEditTriggers);

    locationModel=new QStringListModel(this);
    ui->locationView->setModel(locationModel);
    ui->locationView->setEditTriggers(QListView::NoEditTriggers);

    startSynchronizer();
    updateEntryView();
    updateSiteView();
    updateTopicLocation();
    createMenu();
    loadLocation();
}

Widget::~Widget()
{
    saveLocation();
    stopSynchronizer();
    delete ui;
}

void Widget::generateMenu()
{
    menu->addAction(QStringLiteral("打开视频目录"),
                    this,&Widget::openTopicLocation);
    menu->addSeparator();
    menu->addAction(QStringLiteral("打开下载列表"),
                    this,&Widget::openFetchView);
    menu->addAction(QStringLiteral("打开视频页面"),
                    this,&Widget::openEntrySite);
    menu->addSeparator();
    menu->addAction(QStringLiteral("更新视频列表"),
                    this,&Widget::updateEntryView);
    menu->addAction(QStringLiteral("更新站点列表"),
                    this,&Widget::updateSiteView);
}

void Widget::openEntrySite()
{
    QModelIndex index=ui->entryView->currentIndex();
    if(index.isValid())
    {
        QString label=index.data().toString();
        QString detail=topicLinks.value(label);
        if(!detail.isEmpty())
            QDesktopServices::openUrl(QUrl(detail));
    }
}

void Widget::openTopicLocation()
{
    if(topicDetails.isEmpty())
        return;

    QModelIndex index=ui->entryView->currentIndex();
    if(index.isValid())
    {
        QString label=index.data().toString();
        QString detail=topicDetails.value(label);
        if(detail.isEmpty())
            return;
        if(QDir(detail).exists())
            QDesktopServices::openUrl(QUrl::fromLocalFile(detail));
    }
}

void Widget::updateTopicLocation()
{
    if(!topicDetails.isEmpty())
        topicDetails.clear();

    QSqlQuery query;
    query.exec(QString("select label,detail from %1 where category='Location'")
               .arg(table));
    while (query.next()) {
        QString label=query.value(0).toString();
        QString detail=query.value(1).toString();
        topicDetails.insert(label,detail);
    }
}

void Widget::openFetchView()
{
    QModelIndex index=ui->entryView->currentIndex();
    QString label=index.data().toString();
    QString detail=topicLinks.value(label);
    if(detail.isEmpty())
        return;

    FetchView fetchView(this,label,detail);
    fetchView.move(this->x() + 100,this->y() + 50);
    fetchView.exec();
}

void Widget::updateEntryView()
{
    if(!topicLinks.isEmpty())
        topicLinks.clear();

    QSqlQuery query;
    query.exec(QString("select label,detail from %1 where category='Entry'")
               .arg(table));
    while (query.next()) {
        QString label=query.value(0).toString();
        QString detail=query.value(1).toString();
        topicLinks.insert(label,detail);
    }
    if(!topicLinks.isEmpty())
        entryModel->setStringList(topicLinks.keys());
}

void Widget::updateSiteView()
{
    if(!topicSites.isEmpty())
        topicSites.clear();

    QSqlQuery query;
    query.exec(QString("select label,detail from %1 where category='Site'")
               .arg(table));
    QStringList siteLabels;
    while (query.next()) {
        QString label=query.value(0).toString();
        QString detail=query.value(1).toString();
        siteLabels.append(label);
        topicSites.insert(label,detail);
    }
    if(!siteLabels.isEmpty())
        siteModel->setStringList(siteLabels);
}

void Widget::on_entryView_doubleClicked(const QModelIndex &index)
{
    if(index.isValid())
    {
        QString label=index.data().toString();
        updateLocationView(label);
    }
}

void Widget::updateLocationView(const QString &label)
{
    if(!itemDetails.isEmpty())
        itemDetails.clear();

    QSqlQuery query;
    query.exec(QString("select label,detail from %1 where category='%2'")
               .arg(locationTable)
               .arg(label));

    while (query.next()) {
        QString label=query.value(0).toString();
        QString detail=query.value(1).toString();
        itemDetails.insert(label,detail);
    }
    if(!itemDetails.isEmpty())
        locationModel->setStringList(itemDetails.keys());
}

void Widget::on_siteView_doubleClicked(const QModelIndex &index)
{
    if(index.isValid())
    {
        QString label=index.data().toString();
        QString detail=topicSites.value(label);
        if(!detail.isEmpty())
            QDesktopServices::openUrl(QUrl(detail));
    }
}

void Widget::on_locationView_doubleClicked(const QModelIndex &index)
{
    if(index.isValid())
    {
        QString label=index.data().toString();
        QString detail=itemDetails.value(label);
        if(detail.isEmpty())
            return;

        if(QFile::exists(detail))
            QDesktopServices::openUrl(QUrl::fromLocalFile(detail));
    }
}

void Widget::startSynchronizer()
{
    synchronizer=new LocationSynchronizer;
    connect(&syncThread,&QThread::finished,
            synchronizer,&QObject::deleteLater);
    synchronizer->awake(table,locationTable);
    synchronizer->moveToThread(&syncThread);
    syncThread.start();
}

void Widget::stopSynchronizer()
{
    syncThread.quit();
    syncThread.wait();
}
