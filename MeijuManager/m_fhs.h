﻿#ifndef M_FHS_H
#define M_FHS_H

#include "head/m_head.h"

#define BASE_URL "http://localhost:8080/link?url="

#define T_RES_LINK "res_meiju_link"
#define T_RES_LOCATION "res_meiju_location"
#define T_RES_FETCH "res_meiju_fetch"

/* Category:
 * Entry 普通条目
 * Location 目录，
 * Site 网站条目，
 * Prefix 搜索条目
*/

#define T_RES_TRUNK T_RES_LINK
#define T_RES_BRANCH T_RES_LOCATION

#define THRESHOLD_VALUE 24*3600     //24h

#endif // M_FHS_H
