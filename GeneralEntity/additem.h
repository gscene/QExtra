﻿#ifndef ADDITEM_H
#define ADDITEM_H

#include "ui_additem.h"
#include <head/g_pch.h>
#include <head/db_helper.h>
#include <head/m_message.h>
#include "m_fhs.h"

class AddItem : public QDialog, private Ui::AddItem
{
    Q_OBJECT

public:
    explicit AddItem(const QStringList &topics_,
                     QWidget *parent = nullptr);

    bool addItem(const QString &topic,
                 const QString &label,
                 const QString &detail);

    QString checkFirst(const QString &label);

private slots:
    void on_btn_paste_clicked();
    void on_btn_submit_clicked();
    void on_btn_clean_clicked();
    void on_in_label_editingFinished();

private:
    QString table;
    QStringList topics;
};

#endif // ADDITEM_H
