﻿#ifndef M_FHS_H
#define M_FHS_H

#include <head/m_head.h>

#define G_ENTITY "general_entity"

#define ID_COL 0
#define TOP_COL 1
#define LAB_COL 2
#define DET_COL 3
#define ADD_COL 4

#endif // M_FHS_H
