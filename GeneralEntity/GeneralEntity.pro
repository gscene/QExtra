#-------------------------------------------------
#
# Project created by QtCreator 2018-10-26T15:16:40
#
#-------------------------------------------------

QT       += core gui sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GeneralEntity
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj

CONFIG += c++11

SOURCES += \
        main.cpp \
        widget.cpp \
    ../../elfproj/support/sp_env.cpp \
    additem.cpp

HEADERS += \
        widget.h \
    g_ver.h \
    m_fhs.h \
    ../../elfproj/support/sp_env.h \
    additem.h \
    ../../elfproj/common/baseeditor.h

FORMS += \
        widget.ui \
    additem.ui

RESOURCES += \
    res.qrc

RC_ICONS = tag.ico
