﻿#ifndef WIDGET_H
#define WIDGET_H

#include "ui_widget.h"
#include "additem.h"
#include "common/baseeditor.h"

class Widget : public BaseEditor, private Ui::Widget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);

    void generateMenu();
    void updateTopic();
    void updateView();
    void setHeaderData();
    void newItem();
    void removeItem();

protected:
    void keyPressEvent(QKeyEvent *event);

private slots:
    void on_btn_paste_clicked();
    void on_listView_doubleClicked(const QModelIndex &index);
    void on_kw_returnPressed();

private:
    QString selectTopic;
    QStringList topics;
    QStringListModel *listModel;
};

#endif // WIDGET_H
