﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "GeneralEntity"
#define SP_VER 1009
#define SP_UID "{484a33ad-389b-42ad-8280-28cb4546753d}"
#define SP_TYPE "prop"

#define SP_CFG "GeneralEntity.ini"
#define SP_INFO "GeneralEntity.json"
#define SP_LINK "GeneralEntity.lnk"

/*
 * 1002
 * 1003 自动排序,搜索修正(like)
 * 1004 topic判断
 * 1005 修正了快捷键的bug
 * 1006 增加了空格的判断
 * 1007 checkFirst
 * 1008 on_label_editingFinished
 * 1009 quickConnectFromSHM
*/

#endif // G_VER_H
