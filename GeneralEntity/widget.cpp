﻿#include "widget.h"

Widget::Widget(QWidget *parent) :
    BaseEditor(parent)
{
    setupUi(this);

    table=G_ENTITY;
    listModel=new QStringListModel(this);
    listView->setModel(listModel);

    model=new QSqlTableModel(this);
    tableView->setModel(model);

    updateTopic();
    createMenu();
}

void Widget::updateTopic()
{
    if(!topics.isEmpty())
        topics.clear();

    topics=sp_getTopic(table);
    listModel->setStringList(topics);
}

void Widget::generateMenu()
{
    menu->addAction(QStringLiteral("新记录... (F1)"),this,&Widget::newItem);
    menu->addSeparator();
    menu->addAction(QStringLiteral("更新类别 (F4)"),this,&Widget::updateTopic);
    menu->addAction(QStringLiteral("更新视图 (F5)"),this,&Widget::updateView);
    menu->addSeparator();
    menu->addAction(QStringLiteral("撤销 (Ctrl+Z)"),this,&Widget::revert);
    menu->addAction(QStringLiteral("保存 (Ctrl+S)"),this,&Widget::save);
}

void Widget::keyPressEvent(QKeyEvent *event)
{
    if(event->modifiers() == Qt::ControlModifier && event->key() == Qt::Key_S)
        save();
    else if(event->modifiers() == Qt::ControlModifier && event->key() == Qt::Key_Z)
        revert();
    else {
        switch (event->key()) {
        case Qt::Key_F1:
            newItem();
            break;
        case Qt::Key_F4:
            updateTopic();
            break;
        case Qt::Key_F5:
            updateView();
            break;
        case Qt::Key_Delete:
            removeItem();
            break;
        }
    }
}

void Widget::updateView()
{
    if(!selectTopic.isEmpty())
    {
        model->setTable(table);
        model->setFilter(QString("topic = '%1'").arg(selectTopic));
        model->select();
        setHeaderData();
    }
}

void Widget::setHeaderData()
{
    tableView->hideColumn(ID_COL);
    model->setHeaderData(TOP_COL,Qt::Horizontal,QStringLiteral("系列"));
    model->setHeaderData(LAB_COL,Qt::Horizontal,QStringLiteral("标签"));
    tableView->setColumnWidth(LAB_COL,180);
    model->setHeaderData(DET_COL,Qt::Horizontal,QStringLiteral("详述"));
    tableView->hideColumn(ADD_COL);
    // 排序
    tableView->sortByColumn(LAB_COL,Qt::AscendingOrder);
}

void Widget::newItem()
{
    AddItem itemAdd(topics);
    itemAdd.move(x() + 100,y() + 50);
    itemAdd.exec();
    /*
    if(itemAdd.exec() == QDialog::Accepted)
    {
        updateTopic();
        if(topics.contains(selectTopic))
            updateView();
    }
    */
}

void Widget::removeItem()
{
    QModelIndex index=tableView->currentIndex();
    if(index.isValid())
        model->removeRow(index.row());
}

void Widget::on_btn_paste_clicked()
{
    QString kw=qApp->clipboard()->text().trimmed();
    if(!kw.isEmpty())
        this->kw->setText(kw);
}

void Widget::on_listView_doubleClicked(const QModelIndex &index)
{
    if(index.isValid())
    {
        selectTopic=index.data().toString();
        updateView();
    }
}

void Widget::on_kw_returnPressed()
{
    QString kw=this->kw->text().trimmed();
    if(!kw.isEmpty())
    {
        model->setTable(table);
        model->setFilter(QString("label like '%%1%'").arg(kw));
        model->select();
        setHeaderData();
        this->kw->clear();
    }
}
