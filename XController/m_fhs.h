#ifndef M_FHS_H
#define M_FHS_H

#define A_PRESSED "Pressed"
#define B_RELEASED "Released"

#define A_CONNECTED "Connected"
#define B_DISCONNECTED "Disconnected"

#endif // M_FHS_H
