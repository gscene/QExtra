#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "m_pch.h"
#include "m_fhs.h"

#ifdef WIN32
#include "windows.h"
#endif

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void detectController();
    void append(const QString &action);
    void processAction();

    void onAxisLeftXChanged(double value);
    void onAxisLeftYChanged(double value);
    void onAxisRightXChanged(double value);
    void onAxisRightYChanged(double value);

    void onButtonAChanged(bool pressed);
    void onButtonBChanged(bool pressed);
    void onButtonXChanged(bool pressed);
    void onButtonYChanged(bool pressed);

    void onButtonL1Changed(bool pressed);
    void onButtonL2Changed(double value);
    void onButtonL3Changed(bool pressed);
    void onButtonR1Changed(bool pressed);
    void onButtonR2Changed(double value);
    void onButtonR3Changed(bool pressed);

    void onButtonCenterChanged(bool pressed);
    void onButtonGuideChanged(bool pressed);
    void onButtonSelectChanged(bool pressed);
    void onButtonStartChanged(bool pressed);

    void onButtonUpChanged(bool pressed);
    void onButtonDownChanged(bool pressed);
    void onButtonLeftChanged(bool pressed);
    void onButtonRightChanged(bool pressed);

    void onConnectedChanged(bool connected);
    //  void deviceIdChanged(int value);
    //  void nameChanged(QString value);

private slots:
    void on_mockMouse_stateChanged(int state);
    void on_slider_valueChanged(int value);

private:
    Ui::MainWindow *ui;

    QGamepadManager *m_manager;
    QGamepad *m_gamepad;

    bool mockMouse;
    int mouseMoveOffset;
    int g_height,g_width;
};
#endif // MAINWINDOW_H
