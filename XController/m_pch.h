#ifndef M_PCH_H
#define M_PCH_H

#include <QGuiApplication>
#include <QApplication>
#include <QScreen>
#include <QPoint>
#include <QRect>
#include <QSize>
#include <QDebug>

#include <QWidget>
#include <QMainWindow>
#include <QCursor>
#include <QEvent>
#include <QMouseEvent>
#include <QWheelEvent>

#include <QString>
#include <QStringList>
#include <QList>

#include <QtGamepad/QGamepad>
#include <QtGamepad/QGamepadManager>
#include <QtGamepad/QGamepadKeyNavigation>

#endif // M_PCH_H
