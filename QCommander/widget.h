#ifndef WIDGET_H
#define WIDGET_H

#include "common/baseresteditor.h"
#include "head/g_functionbase.h"
#include <QTextCursor>
#include <QTextCharFormat>

#include "element/bookmarks.h"
#include "element/commandstore.h"
#include "common/user.h"

namespace Ui {
class Widget;
}

class Widget : public BaseRestEditor
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();

    void customKeyEvent(QKeyEvent *event)
    {
        Q_UNUSED(event)
    }

    void onReceivedObject(const QJsonObject &object);

    void ping();
    void say(const QString &word);
    void append(const QString &word);
    void clear();

    bool db_try();
    void executeCommand(int code);

private slots:
    void on_btn_submit_clicked();

private:
    Ui::Widget *ui;

    QTextCharFormat defaultTextFormat;
    QSqlDatabase db;
    CommandStore cmdStore;
    Bookmarks bookmarks;
    User user;
    QString input_;
};

#endif // WIDGET_H
