#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "QCommander"
#define SP_VER 1002
#define SP_UID "{2933f6b2-61b1-45de-8fb2-ee98a8dffd75}"
#define SP_TYPE "shell"

#define SP_CFG "QCommander.ini"
#define SP_INFO "QCommander.json"
#define SP_LINK "QCommander.lnk"

/*
 * 1002 基于TulingBox
*/

#endif // G_VER_H
