#-------------------------------------------------
#
# Project created by QtCreator 2019-07-14T09:01:57
#
#-------------------------------------------------

QT       += core gui sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = QCommander
TEMPLATE = app

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        main.cpp \
    ../../elfproj/support/sp_env.cpp \
        widget.cpp

HEADERS += \
    ../../elfproj/common/baseresteditor.h \
    ../../elfproj/support/sp_env.h \
    ../../elfproj/element/bookmarks.h \
    ../../elfproj/element/commandstore.h \
    ../../elfproj/common/user.h \
    g_ver.h \
    m_fhs.h \
        widget.h

FORMS += \
        widget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
