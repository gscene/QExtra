﻿#include "belltitle.h"

BellTitle::BellTitle(QWidget *parent) :
    QWidget(parent)
{
    titleText=new QLabel(this);

    QHBoxLayout *layout=new QHBoxLayout;
    layout->addWidget(titleText);
    setLayout(layout);
    setFixedHeight(30);
}

void BellTitle::paintEvent(QPaintEvent *)
{
    QLinearGradient linear(rect().topLeft(),rect().bottomRight());
    linear.setColorAt(0,QColor(227,207,87));
    linear.setColorAt(0.5,QColor(245,222,179));
    linear.setColorAt(1,QColor(189,252,201));

    QPainter painter(this);
    painter.setBrush(QBrush(linear));
    painter.setPen(Qt::NoPen);
    painter.drawRect(rect());
}

void BellTitle::setTitleText(QString title)
{
    QFont font=this->font();
    font.setPointSize(9);
    titleText->setFont(font);
    titleText->setText(title);
}
