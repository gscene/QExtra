﻿#ifndef BELLTITLE_H
#define BELLTITLE_H

#include <QWidget>
#include <QLabel>
#include <QPaintEvent>
#include <QHBoxLayout>
#include <QPainter>
#include <QLinearGradient>
#include <QFont>

class BellTitle : public QWidget
{
    Q_OBJECT
public:
    explicit BellTitle(QWidget *parent = 0);

    void setTitleText(QString title);
    QLabel *titleText;

protected:
    void paintEvent(QPaintEvent *);
};

#endif // BELLTITLE_H
