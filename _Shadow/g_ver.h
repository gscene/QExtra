﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "_Shadow"
#define SP_VER 1002
#define SP_UID "{f2360d09-7d9f-48e7-9153-f613771ace42}"
#define SP_TYPE "support"

#define SP_CFG "_Shadow.ini"
#define SP_INFO "_Shadow.json"
#define SP_LINK "_Shadow.lnk"

/*
 * 1001
 * 1002 屏幕录制功能
*/
#endif // G_VER_H
