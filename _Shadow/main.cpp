﻿#include "shadow.h"
#include <QtSingleApplication>
#include "head/g_bootfunction.h"

int main(int argc, char *argv[])
{
    qInstallMessageHandler(sp_messageLogHandler);
    QtSingleApplication  a(SP_NAME,argc, argv);

    int ret = sp_parseArgument(argc,argv);
    if(ret != RET_CONTINUE)
        return ret;
    if(a.isRunning())
    {
        qInfo() << "exit by another is running";
        return EXIT_SUCCESS;
    }

    a.setQuitOnLastWindowClosed(false);

    Shadow w;
    a.setActivationWindow(&w);
    //w.hide();
    w.show();

    return a.exec();
}
