﻿#ifndef SHADOW_H
#define SHADOW_H

#include <QWidget>
#include <QScreen>
#include <QPixmap>
#include <QTimerEvent>
#include <QDebug>

#include "support/basereceiver.h"
#include "module/bell.h"

#include <QHostAddress>
#include "m_fhs.h"

class Shadow : public QWidget
{
    Q_OBJECT

public:
    Shadow(QWidget *parent = 0);
    ~Shadow();

    void ring(const QString &title, const QString &content);
    void startReceiver();
    void endReceiver();

    void timerEvent(QTimerEvent *event);
    void captureToDatagram();

    void startScreenRecorder(int msec=50);
    void stopScreenRecorder();

    void received(const QByteArray &data);

private:
    bool isScreenRecorder;

    BaseReceiver *receiver;
    QThread recvThread;

    QHostAddress addr;
    QScreen *screen;

    int screenRecorderId;
};

#endif // SHADOW_H
