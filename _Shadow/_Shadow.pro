#-------------------------------------------------
#
# Project created by QtCreator 2017-02-11T15:25:21
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = _Shadow
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

include(../qtsingleapplication/src/qtsingleapplication.pri)

INCLUDEPATH += ../../elfproj

SOURCES += main.cpp\
        shadow.cpp \
    ../../elfproj/support/basereceiver.cpp \
    module/bell.cpp \
    module/belltitle.cpp \
    ../../elfproj/support/sp_env.cpp

HEADERS  += shadow.h \
    ../../elfproj/support/basereceiver.h \
    m_fhs.h \
    module/bell.h \
    module/belltitle.h \
    g_ver.h \
    ../../elfproj/support/sp_env.h
