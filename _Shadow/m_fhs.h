﻿#ifndef M_FHS_H
#define M_FHS_H

#define RECV_PORT 23329
#define CAPTURE_PORT 23327

#include "g_ver.h"
#include "head/g_fhs.h"

#define APP_ROOT "/QVertex"
#define LOCATION_FILE "/location.ini"

#endif // M_FHS_H
