#-------------------------------------------------
#
# Project created by QtCreator 2015-11-03T19:54:33
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = Whisper
TEMPLATE = app

INCLUDEPATH += ../../elfproj

SOURCES += main.cpp\
        dialog.cpp \
    ../../elfproj/support/pie.cpp

HEADERS  += dialog.h \
    g_ver.h \
    ../../elfproj/support/pie.h

FORMS    += dialog.ui
