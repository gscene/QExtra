﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "Whisper"
#define SP_VER 1001
#define SP_UID "{f19a85e5-d142-42c6-9aaa-787f550a0413}"
#define SP_TYPE "shell"

#define SP_LINK "Whisper.lnk"
#define SP_INFO "Whisper.json"

/*
 * 1001 项目开始
*/

#endif // G_VER_H
