﻿#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QString>
#include <QSettings>
#include <QByteArray>
#include <QUrl>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QMessageBox>
#include <QFile>
#include <QDebug>
#include <QMap>

#include "head/g_function.h"
#include "support/pie.h"

#define CFG_FILE "../etc/Whisper.ini"

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    enum RemoteRequest {
        R_Get,
        R_Post
    };

    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

    void ping();
    void replyFinished(QNetworkReply *);
    void get(const QString &url);

private slots:
    void down();
    void on_submit_clicked();
    void on_ask_clicked();

private:
    Ui::Dialog *ui;
    QString base_url,url;
    QNetworkAccessManager *manager;
    QNetworkReply *pingReply;
    QString key,value;
    QMap<QNetworkReply *, RemoteRequest> replyMap;
};

#endif // DIALOG_H
