﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "GeneralOperator"
#define SP_VER 1003
#define SP_UID "{f1b5aedc-b108-4f01-adf6-f04c4c9e3afe}"
#define SP_TYPE "extra"
#define SP_VERSION "[Version " + QString::number(SP_VER) + QString("]")

#define SP_CFG "GeneralOperator.ini"
#define SP_INFO "GeneralOperator.json"
#define SP_LINK "GeneralOperator.lnk"

/*
 * 1002 rename,fill
 * 1003 default download location
 */

#endif // G_VER_H
