#-------------------------------------------------
#
# Project created by QtCreator 2017-04-13T17:39:19
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = GeneralOperator
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj

SOURCES += main.cpp\
        widget.cpp \
    ../../elfproj/support/sp_env.cpp

HEADERS  += widget.h \
    m_fhs.h \
    g_ver.h \
    ../../elfproj/support/sp_env.h

FORMS    += widget.ui
