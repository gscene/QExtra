﻿#ifndef WIDGET_H
#define WIDGET_H

#include "support/sp_env.h"
#include "m_fhs.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

    void display(const QString &text);
    void operate_fetch(const QFileInfo &fileInfo);
    void operate_rename();
    void operate_fill(const QString &baseName);

private slots:
    void on_btn_pick_clicked();
    void on_btn_rename_clicked();
    void on_btn_fill_clicked();

private:
    Ui::Widget *ui;

    QMap<QString,QString> labelCache;      //fileName,label
    QMap<QString,QFileInfo> infoCache;     //fileName,fileInfo
};

#endif // WIDGET_H
