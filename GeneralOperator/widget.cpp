﻿#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    ui->location->setText(Location::download);
    display(QStringLiteral("欢迎使用——"));
    display(QStringLiteral("当前目录已设置为：") + Location::download);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::display(const QString &text)
{
    ui->display->append(text);
}

void Widget::on_btn_pick_clicked()
{
    QString location=QFileDialog::getExistingDirectory(this,QStringLiteral("选择目录"));
    if(location.isEmpty())
        return;

    ui->location->setText(location);
    display(QStringLiteral("当前目录已设置为：") + location);
}

void Widget::on_btn_rename_clicked()
{
    QString location=ui->location->text().trimmed();
    if(location.isEmpty())
        location=Location::download;

    QDir dir(location);
    if(dir.isRelative())
        if(!dir.makeAbsolute())
        {
            display(QStringLiteral("目录无效，无法继续操作"));
            return;
        }

    display(QStringLiteral("开始文件夹遍历……"));
    if(!labelCache.isEmpty())
        labelCache.clear();

    if(!infoCache.isEmpty())
        infoCache.clear();

    QDirIterator dirIter(dir.path(),QDir::Files | QDir::NoSymLinks,
                         QDirIterator::Subdirectories);
    while (dirIter.hasNext()) {
        dirIter.next();
        operate_fetch(dirIter.fileInfo());
    }

    if(!labelCache.isEmpty())
        operate_rename();
    else
        display(QStringLiteral("目录为空，无需操作"));
}

void Widget::operate_fetch(const QFileInfo &fileInfo)
{
    QString fileName=fileInfo.fileName();
    QSqlQuery query;
    query.exec(QString("select label from %1 where detail like '%%2%'")
               .arg(TD_FETCH)
               .arg(fileName));
    if(query.next())
    {
        QString label=query.value("label").toString();
        operate_fill(label);

        labelCache.insert(fileName,label);
        infoCache.insert(fileName,fileInfo);
    }
}

void Widget::operate_rename()
{
    display(QStringLiteral("开始重命名操作……"));
    QMap<QString,QString>::ConstIterator itor;
    for(itor = labelCache.constBegin();itor != labelCache.constEnd(); itor ++)
    {
        QString fileName=itor.key();
        QString label=itor.value();

        QFileInfo fileInfo=infoCache.value(fileName);
        QString path=fileInfo.path();
        QString suffix=fileInfo.suffix();

        QString targetName=label+ "." + suffix;
        QString target=path + "/" +targetName;

        display(fileName + " ----> " + targetName);
        if(!QFile::rename(fileInfo.filePath(),target))
            display(QStringLiteral("无法操作：") + fileName);
    }
}

void Widget::operate_fill(const QString &baseName)
{
    QSqlQuery query;
    QString sql=QString("update %1 set status='Finish' where label='%2'").arg(TD_FETCH).arg(baseName);
    if(query.exec(sql))
        display(QStringLiteral("补全：") + baseName);
    else
        display(query.lastError().text());
}

void Widget::on_btn_fill_clicked()
{
    QString location=ui->location->text().trimmed();
    if(location.isEmpty())
    {
        display(QStringLiteral("目录不存在，无法继续操作"));
        return;
    }

    QDir dir(location);
    if(dir.isRelative())
        if(!dir.makeAbsolute())
        {
            display(QStringLiteral("目录无效，无法继续操作"));
            return;
        }

    display(QStringLiteral("开始文件夹遍历……"));
    QDirIterator dirIter(dir.path(),QDir::Files | QDir::NoSymLinks,
                         QDirIterator::Subdirectories);
    while (dirIter.hasNext()) {
        dirIter.next();
        QString baseName=dirIter.fileInfo().baseName();
        QSqlQuery query;
        QString sql=QString("select label from %1 where label='%2'").arg(TD_FETCH).arg(baseName);
        query.exec(sql);
        if(query.next())
            operate_fill(baseName);
    }
}
