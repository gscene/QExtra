﻿#ifndef ITEMADD_H
#define ITEMADD_H

#include <QDialog>
#include <QMessageBox>
#include <QString>
#include <QClipboard>
#include <QSqlQuery>

#include "m_fhs.h"

namespace Ui {
class ItemAdd;
}

class ItemAdd : public QDialog
{
    Q_OBJECT

public:
    explicit ItemAdd(QWidget *parent = 0);
    ~ItemAdd();

    bool addItem(const QString &label, const QString &url,
                 const QString &hash, const QString &ref);

private slots:
    void on_btn_clear_clicked();

    void on_btn_paste_clicked();

    void on_btn_submit_clicked();

private:
    Ui::ItemAdd *ui;
};

#endif // ITEMADD_H
