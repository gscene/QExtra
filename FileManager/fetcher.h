﻿#ifndef FETCHER_H
#define FETCHER_H

#include "common/baseeditor.h"
#include "itemadd.h"

namespace Ui {
class Fetcher;
}

class Fetcher : public BaseEditor
{
    Q_OBJECT

public:
    explicit Fetcher(QWidget *parent = 0);
    ~Fetcher();

    void updateView();
    void createMenu();
    void search();

    void addItem();
    void removeItem();
    void copyHash();

private slots:
    void on_tableView_clicked(const QModelIndex &index);

    void on_tableView_doubleClicked(const QModelIndex &index);

private:
    Ui::Fetcher *ui;
};

#endif // FETCHER_H
