#-------------------------------------------------
#
# Project created by QtCreator 2016-09-25T19:03:42
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = FileManager
TEMPLATE = app

include(../qtsingleapplication/src/qtsingleapplication.pri)

INCLUDEPATH += ../../elfproj

SOURCES += main.cpp\
        manager.cpp \
    ../../elfproj/common/baseeditor.cpp \
    fetcher.cpp \
    itemadd.cpp

HEADERS  += manager.h \
    ../../elfproj/common/baseeditor.h \
    m_fhs.h \
    g_ver.h \
    fetcher.h \
    itemadd.h

FORMS    += manager.ui \
    fetcher.ui \
    itemadd.ui

RESOURCES += \
    res.qrc

RC_ICONS = manager.ico
