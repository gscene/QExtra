﻿#include "itemadd.h"
#include "ui_itemadd.h"

ItemAdd::ItemAdd(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ItemAdd)
{
    ui->setupUi(this);
}

ItemAdd::~ItemAdd()
{
    delete ui;
}

void ItemAdd::on_btn_clear_clicked()
{
    ui->url->clear();
}

void ItemAdd::on_btn_paste_clicked()
{
    QString url=qApp->clipboard()->text();
    ui->url->setPlainText(url);
}

bool ItemAdd::addItem(const QString &label, const QString &url, const QString &hash, const QString &ref)
{
    QSqlQuery query;
    query.prepare(QString("insert into %1 (label,url,hash,ref) values (?,?,?,?)").arg(TD_FETCH));
    query.addBindValue(label);
    query.addBindValue(url);
    query.addBindValue(hash);
    query.addBindValue(ref);
    return query.exec();
}

void ItemAdd::on_btn_submit_clicked()
{
    QString label=ui->label->text().trimmed();
    if(label.isEmpty())
        return;

    QString url=ui->url->toPlainText();
    if(url.isEmpty())
        return;

    QString hash=ui->hash->text().trimmed();
    QString ref=ui->ref->text().trimmed();

    if(!addItem(label,url,hash,ref))
        QMessageBox::warning(this,QStringLiteral("异常情况"),QStringLiteral("无法提交数据！"));
    else
        accept();
}
