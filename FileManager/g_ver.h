﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "FileManager"
#define SP_VER 1005
#define SP_UID "{f7cf0e07-0b4a-44bc-a00b-5acb9ef45f46}"
#define SP_TYPE "shell"

#define SP_CFG "FileManager.ini"
#define SP_INFO "FileManager.json"
#define SP_LINK "FileManager.lnk"

/*
 * 1001 项目开始
 * 1002 和GM共用组件
 * 1003 修正iteamadd 插入bug
 * 1004 #13
 * 1005 #18
*/

#endif // G_VER_H
