﻿#include "fetcher.h"
#include "ui_fetcher.h"

Fetcher::Fetcher(QWidget *parent) :
    BaseEditor(parent),
    ui(new Ui::Fetcher)
{
    ui->setupUi(this);

    table=TD_FETCH;

    model=new QSqlTableModel(this);
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    ui->tableView->setModel(model);

    updateView();
    createMenu();
}

Fetcher::~Fetcher()
{
    delete ui;
}

void Fetcher::createMenu()
{
    menu=new QMenu(this);
    menu->addAction(QStringLiteral("添加..."),this,&Fetcher::addItem);
    menu->addAction(QStringLiteral("查找..."),this,&Fetcher::search);
    menu->addSeparator();
    menu->addAction(QStringLiteral("更新视图"),this,&Fetcher::updateView);
    menu->addAction(QStringLiteral("复制校验码"),this,&Fetcher::copyHash);
    menu->addSeparator();
    menu->addAction(QStringLiteral("保存"),this,&Fetcher::save);
    menu->addAction(QStringLiteral("撤销"),this,&Fetcher::revert);
    menu->addSeparator();
    menu->addAction(QStringLiteral("删除"),this,&Fetcher::removeItem);
}

void Fetcher::search()
{
    showSearchBox("label");
}

void Fetcher::addItem()
{
    ItemAdd item;
    if(item.exec() == QDialog::Accepted)
        updateView();
}

void Fetcher::removeItem()
{
    if(ui->tableView->currentIndex().isValid())
    {
        int row=ui->tableView->currentIndex().row();
        model->removeRow(row);
    }
}

void Fetcher::updateView()
{
    model->setTable(table);
    model->setSort(0,Qt::DescendingOrder);
    model->select();

    ui->tableView->hideColumn(0);   //id
    model->setHeaderData(1,Qt::Horizontal,QStringLiteral("文件名|标题"));    //label
    ui->tableView->setColumnWidth(1,480);
    ui->tableView->hideColumn(2);   //url
    model->setHeaderData(3,Qt::Horizontal,QStringLiteral("来源"));    //ref
    model->setHeaderData(4,Qt::Horizontal,QStringLiteral("状态"));    //status
    model->setHeaderData(5,Qt::Horizontal,QStringLiteral("校验码"));   //hash
    ui->tableView->hideColumn(6);   //type
}

void Fetcher::on_tableView_clicked(const QModelIndex &index)
{
    if(index.isValid())
    {
        QSqlRecord record=model->record(index.row());
        QString url=record.value("url").toString();
        ui->url->setPlainText(url);
    }
}

void Fetcher::copyHash()
{
    QModelIndex index=ui->tableView->currentIndex();
    if(index.isValid())
    {
        QSqlRecord record=model->record(index.row());
        QString hash=record.value("hash").toString();
        qApp->clipboard()->setText(hash);
        QMessageBox::information(this,QStringLiteral("已复制"),QStringLiteral("校验码已复制到剪贴板"));
    }
}

void Fetcher::on_tableView_doubleClicked(const QModelIndex &index)
{
    if(index.isValid())
    {
        QSqlRecord record=model->record(index.row());
        QString url=record.value("url").toString();
        qApp->clipboard()->setText(url);
 //       QMessageBox::information(this,QStringLiteral("已复制"),QStringLiteral("链接已复制到剪贴板"));
    }
}
