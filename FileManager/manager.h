﻿#ifndef MANAGER_H
#define MANAGER_H

#include <QWidget>
#include "fetcher.h"

namespace Ui {
class Manager;
}

class Manager : public QWidget
{
    Q_OBJECT

public:
    explicit Manager(QWidget *parent = 0);
    ~Manager();

private:
    Ui::Manager *ui;

    Fetcher *fetcher;
};

#endif // MANAGER_H
