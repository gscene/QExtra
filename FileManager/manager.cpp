﻿#include "manager.h"
#include "ui_manager.h"

Manager::Manager(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Manager)
{
    ui->setupUi(this);

    fetcher=new Fetcher(this);
    ui->tabWidget->addTab(fetcher,QStringLiteral("下载"));
}

Manager::~Manager()
{
    delete ui;
}
