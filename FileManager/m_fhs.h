﻿#ifndef M_FHS_H
#define M_FHS_H

#include "g_ver.h"
#include "head/g_fhs.h"

#define M_CONFIG QString(FHS_ETC) + SP_CFG
#define DB_CONFIG "../etc/local.ini"

#define TD_FETCH "file_fetch"
#define TD_LOCATION "file_location"

#endif // M_FHS_H
