﻿#include "popup.h"
#include "ui_popup.h"

Popup::Popup(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Popup)
{
    ui->setupUi(this);

    // setAttribute(Qt::WA_ShowWithoutActivating);

    setWindowFlags(
                Qt::FramelessWindowHint | // No window border
                //           Qt::WindowDoesNotAcceptFocus | // No focus
                Qt::WindowStaysOnTopHint |// Always on top
                Qt::Tool
                );

    QRect flatRect=qApp->desktop()->availableGeometry();
    QSize flatSize(flatRect.width(),flatRect.height() / 3);
    setGeometry(QStyle::alignedRect(
                    Qt::LeftToRight,
                    Qt::AlignBottom,
                    flatSize,
                    flatRect));
}

Popup::~Popup()
{
    delete ui;
}

void Popup::showMe(const QString &detail, bool append)
{
    if(append)
    {
        QString text=ui->content->text();
        text.append(detail);
        ui->content->setText(text);
    }
    else
        ui->content->setText(detail);
    /*
    setGeometry(QStyle::alignedRect(
                    Qt::RightToLeft,
                    Qt::AlignBottom,
                    size(),
                    qApp->desktop()->availableGeometry()));
*/
    if(!isVisible())
        show();
}

void Popup::mouseDoubleClickEvent(QMouseEvent *)
{
    close();
}
