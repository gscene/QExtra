﻿#ifndef POPUP_H
#define POPUP_H

#include <QDialog>
#include <QDesktopWidget>
#include <QMouseEvent>

namespace Ui {
class Popup;
}

class Popup : public QDialog
{
    Q_OBJECT

public:
    explicit Popup(QWidget *parent = 0);
    ~Popup();

    void showMe(const QString &detail,bool append=false);
    void mouseDoubleClickEvent(QMouseEvent *);

private:
    Ui::Popup *ui;
};

#endif // POPUP_H
