﻿#ifndef WIDGET_H
#define WIDGET_H

#include "support/sp_env.h"
#include "element/basereceiver.h"
#include "boot/g_bootfunction.h"
#include "head/m_function.h"
#include "common/user.h"

#include "m_fhs.h"
#include "support/ref_commands.h"
#include "module/popup.h"

class Scheduler : public QWidget
{
    Q_OBJECT

public:
    Scheduler(QWidget *parent = 0);
    ~Scheduler();
    
    bool db_try();
    void updateTimeLine();
    void startReceiver();
    void received(const QByteArray &data);

    void showPopup(const QString &detail,bool append=false);
    void processActions(const QStringList actions);

protected:
    void timerEvent(QTimerEvent *event);

private:
    int dayNo;
    QSqlDatabase db;
    QString table;
    int autoSync_timer;
    int dbTry_failure;
    QMap<int,QTime> timeLine;

    BaseReceiver *receiver;
    QThread recvThread;

    Popup *popup;
    User user;
};

#endif // WIDGET_H
