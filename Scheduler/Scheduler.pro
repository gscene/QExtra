#-------------------------------------------------
#
# Project created by QtCreator 2017-03-27T08:24:58
#
#-------------------------------------------------

QT       += core gui sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = Scheduler
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj
INCLUDEPATH += ../common

SOURCES += main.cpp\
    ../../elfproj/support/sp_env.cpp \
    scheduler.cpp \
    module/popup.cpp

HEADERS  += \
    ../../elfproj/support/sp_env.h \
    ../common/m_fhs.h \
    g_ver.h \
    ../../elfproj/support/basereceiver.h \
    scheduler.h \
    module/popup.h \
    ../../elfproj/common/user.h

FORMS += \
    module/popup.ui
