﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "Scheduler"
#define SP_VER 1012
#define SP_UID "{9d2c93d8-a0f2-4de7-9f25-fb1e2e121f6b}"
#define SP_TYPE "support"

#define SP_CFG "Scheduler.ini"
#define SP_INFO "Scheduler.json"
#define SP_LINK "Scheduler.lnk"

/*
 * 1002 指令模式
 * 1003 自动db_try
 * 1004 remind,open,execute
 * 1005 自动更新TimeLine
 * 1006 Rect
 * 1007 timerEvent
 * 1008 manager分离
 * 1009 统一指令集
 * 1010 修正sync
 * 1011 week
 * 1012 m_function
 *
 */
#endif // G_VER_H
