﻿#include "scheduler.h"
#include <QtSingleApplication>
#include "head/e_boot.h"

int main(int argc, char *argv[])
{
    SP_Single_Boot_Quiet
            a.setQuitOnLastWindowClosed(false);

    qInstallMessageHandler(sp_messageLogHandler);

    Scheduler w;
    a.setActivationWindow(&w);
    w.hide();

    return a.exec();
}
