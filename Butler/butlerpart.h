﻿#ifndef BUTLERPART_H
#define BUTLERPART_H

#include <head/g_pch.h>
#include "g_ver.h"
#include "m_fhs.h"
#include "element/baseudpreceiver.h"
#include "entity/intent.pb.h"

class ButlerPart : public QWidget
{
    Q_OBJECT

public:
    ButlerPart(QWidget *parent = 0);
    ~ButlerPart();

    void createTray();
    void startReceiver();
    void stopReceiver();
    void sendLocalMessage(const QString &message);
    void onReceived(QByteArray data);

private:
    QSystemTrayIcon *tray;
    QMenu *trayMenu;

    BaseUdpReceiver *receiver;
    QThread recvThread;
};

#endif // BUTLERPART_H
