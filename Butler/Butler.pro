#-------------------------------------------------
#
# Project created by QtCreator 2018-11-15T13:36:44
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Butler
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

include(../qtsingleapplication/src/qtsingleapplication.pri)
INCLUDEPATH += ../../elfproj
LIBS += -lprotobuf

CONFIG += c++11

SOURCES += \
        main.cpp \
        butlerpart.cpp \
    ../../elfproj/support/sp_env.cpp \
    entity/intent.pb.cc

HEADERS += \
        butlerpart.h \
    ../../elfproj/support/sp_env.h \
    g_ver.h \
    m_fhs.h \
    ../../elfproj/element/baseudpreceiver.h \
    entity/intent.pb.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    res.qrc

RC_ICONS = finder.ico
