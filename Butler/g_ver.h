﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "Butler"
#define SP_VER 1002
#define SP_UID "{12afb381-8ec7-4661-bc23-f5d7236561f9}"
#define SP_TYPE "extra"
#define SP_VERSION "[Version " + QString::number(SP_VER) + QString("]")

#define SP_CFG "Butler.ini"
#define SP_INFO "Butler.json"
#define SP_LINK "Butler.lnk"

/*
 * 1002 基本接收器框架
*/

#endif // G_VER_H
