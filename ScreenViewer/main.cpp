#include "screenviewer.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ScreenViewer w;
    w.show();

    return a.exec();
}
