﻿#ifndef SCREENVIEWER_H
#define SCREENVIEWER_H

#include <QDialog>
#include <QUdpSocket>
#include <QByteArray>
#include <QDataStream>
#include <QPixmap>

#include "support/basereceiver.h"

namespace Ui {
class ScreenViewer;
}

class ScreenViewer : public QDialog
{
    Q_OBJECT

public:
    explicit ScreenViewer(QWidget *parent = 0);
    ~ScreenViewer();

    void startReceiver();
    void endReceiver();
    void received(const QByteArray &data);

private:
    Ui::ScreenViewer *ui;

    BaseReceiver *receiver;
    QThread recvThread;
};

#endif // SCREENVIEWER_H
