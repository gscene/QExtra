﻿#include "screenviewer.h"
#include "ui_screenviewer.h"

ScreenViewer::ScreenViewer(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ScreenViewer)
{
    ui->setupUi(this);

    startReceiver();
}

ScreenViewer::~ScreenViewer()
{
    delete ui;
}

void ScreenViewer::startReceiver()
{
    receiver=new BaseReceiver(0,CAPTURE_PORT);
    connect(receiver,&BaseReceiver::received,this,&ScreenViewer::received);
    connect(&recvThread,&QThread::finished,receiver,&QObject::deleteLater);
    receiver->moveToThread(&recvThread);
    recvThread.start();
}

void ScreenViewer::endReceiver()
{
    recvThread.quit();
    recvThread.wait();
}

void ScreenViewer::received(const QByteArray &data)
{
    QPixmap pix;
    QDataStream in(data);
    in >> pix;

    if(pix.isNull())
        return;

    ui->display->setPixmap(pix.scaledToWidth(ui->display->width()));
}
