﻿#ifndef EXPLORER_H
#define EXPLORER_H

#include <QMainWindow>
#include "head/g_pch.h"

namespace Ui {
class Explorer;
}

class Explorer : public QMainWindow
{
    Q_OBJECT

public:
    explicit Explorer(QWidget *parent = 0);
    ~Explorer();

    void createMenu();
    void updateRootView();

protected:
    void contextMenuEvent(QContextMenuEvent *event);

private slots:
    void on_treeView_doubleClicked(const QModelIndex &index);

private:
    Ui::Explorer *ui;

    QMenu *menu;
    QFileSystemModel *model;
    QMimeDatabase mimeDB;
};

#endif // EXPLORER_H
