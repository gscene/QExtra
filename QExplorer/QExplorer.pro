#-------------------------------------------------
#
# Project created by QtCreator 2017-08-27T14:21:34
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
TARGET = QExplorer
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH += ../../elfproj

SOURCES += \
        main.cpp \
        explorer.cpp \
    filemanager.cpp

HEADERS += \
        explorer.h \
    m_fhs.h \
    g_ver.h \
    filemanager.h

FORMS += \
        explorer.ui
