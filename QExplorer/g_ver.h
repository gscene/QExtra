﻿#ifndef G_VER_H
#define G_VER_H

#define SP_NAME "QExplorer"
#define SP_VER 1002
#define SP_UID "{b9a71204-26df-408b-8123-34e0fe46b234}"
#define SP_TYPE "shell"
#define SP_VERSION "[Version " + QString::number(SP_VER) + QString("]")

#define SP_CFG "QExplorer.ini"
#define SP_INFO "QExplorer.json"
#define SP_LINK "QExplorer.lnk"

/*
 * 1002
*/
#endif // G_VER_H
