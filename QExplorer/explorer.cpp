﻿#include "explorer.h"
#include "ui_explorer.h"

Explorer::Explorer(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Explorer)
{
    ui->setupUi(this);

    model=new QFileSystemModel(this);
    ui->treeView->setModel(model);
    updateRootView();
}

Explorer::~Explorer()
{
    delete ui;
}

void Explorer::createMenu()
{
    menu=new QMenu(this);
}

void Explorer::contextMenuEvent(QContextMenuEvent *event)
{
    menu->exec(QCursor::pos());
}

void Explorer::updateRootView()
{
    model->setRootPath(".");
    ui->treeView->hideColumn(1);
    ui->treeView->hideColumn(2);
    ui->treeView->hideColumn(3);
    ui->treeView->setHeaderHidden(true);
}

void Explorer::on_treeView_doubleClicked(const QModelIndex &index)
{
    if(!model->isDir(index))
    {
        QMimeType mimeType=mimeDB.mimeTypeForFile(model->fileName(index),
                                                  QMimeDatabase::MatchExtension);
        QString fileType=mimeType.name();
        ui->statusBar->showMessage(fileType);
    }
}
