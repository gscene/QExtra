﻿#ifndef M_FHS_H
#define M_FHS_H

#include "head/m_head.h"
#include "template/m_sql.h"

#define TEXT_REMIND QStringLiteral("提醒")
#define TEXT_OPEN QStringLiteral("打开文件")
#define TEXT_EXECUTE QStringLiteral("执行命令")

#define ACTION_REMIND 1
#define ACTION_OPEN 2
#define ACTION_EXECUTE 3

#endif // M_FHS_H
